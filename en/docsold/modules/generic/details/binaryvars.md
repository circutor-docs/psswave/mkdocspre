# Binary variables

In the **BINARY VARIABLES** tab, you can enter as many binary variables from the modbus map as you want.

![](/en/_custom/img/generic_tab_binvars_main.jpg)

If you choose the “New variable” action, a dialogue box opens that allows you to configure it.

## General

![](/en/_custom/img/generic_binvars_general.jpg)

* **Identifier**: Name you want to use to identify the variable.
* **Name**: Name of the variable.
* **Description**: Additional information on a variable (optional).
* **Type**: Type of variable. It can be read or write (information from the manufacturer’s modbus map).
* **Read function code**: Function code to read the variable (information from the manufacturer’s modbus map).
* **Write function code**: Function code to write the variable (information from the manufacturer’s modbus map).

## Metadata

![](/en/_custom/img/generic_binvars_metadata.jpg)

The metadata provide information on the variable so that PowerStudio can categorise it:

* **Family**: This tells the type of measure you are doing (if not available in the drop-down menu, you can indicate a specific one).
* **Order**: This tells the order of the variable (available for harmonic variables).

## Save and test

![](/en/_custom/img/generic_binvars_test.jpg)

* **Save:** This tells if you want to save the values of this variable in the PowerStudio database.
* **Test**: If you have properly configured the “modbus testing parameters” section described above, by choosing the icon it will display the value of the variable you have configured to check that the programming is correct.