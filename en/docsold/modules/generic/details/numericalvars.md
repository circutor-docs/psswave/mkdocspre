# Numerical variables

In the **NUMERICAL VARIABLES** tab, you can enter as many numerical values from the modbus map as you want.

![](/en/_custom/img/generic_tab_numvars_main.jpg)

If you choose the “New variables” action, a dialogue box opens that enables you to configure it.

## General

![](/en/_custom/img/generic_numvars_general.jpg)

* **Identifier**: Name you want to use to identify the variable.
* **Name**: Name of the variable.
* **Description**: Additional information on a variable (optional).
* **Type**: Type of variable. The options are read or write (information from the manufacturer’s modbus map).
* **Read function code**: Function code to read the variable (information from the manufacturer’s modbus map).
* Write function code: Function code to write the variable (information from the manufacturer’s modbus map).
* **Information on forcing**: additional information to be shown when forcing the variable (such as permitted values for the variable).
* **Address**: Memory address of the variable (information from the manufacturer’s modbus map).
* **Records**: number of records of the variable: (information from the manufacturer’s modbus map).

## Format and units

![](/en/_custom/img/generic_numvars_format.jpg)

* **Decimals**: number of decimals with which the variable is shown (information from the manufacturer’s modbus map). In this case, it is 3, given that the manufacture indicates a factor of 0.001.
* **IEE754**: Mark when it is a Float type variable (information from the manufacturer’s modbus map).
* **Mod10**: Modbus variable format used by some manufacturers (information from the manufacturer’s modbus map).
* **With sign**: This tells whether the variable has a sign (information from the manufacturer’s modbus map).
* **Units**: This indicates the units of the variable (information from the manufacturer’s modbus map). If the unit is not in the drop-down menu, choose “Defined by user” and enter it manually.

## Grouping criterion

This is the criterion used to group the data:

* **Mean value**: The mean value of the values received will be saved.
* **Last value**: The last value received will be saved.
* **Differential**: The value as the incremental of the period will be saved.

## Metadata

![](/en/_custom/img/generic_numvars_metadata.jpg)

The metadata provide information on the variable so that PowerStudio can categorise it:

* **Family**: This tells the type of measure you are doing (if not available in the drop-down menu, you can indicate a specific one).
* **Order**: This tells the order of the variable (available for harmonic variables).
* **Phase**: This tells the phase to which the variable belongs.
* **Type**: This is the type of variable; it can be: **Instantaneous**: It is a mean-type variable. **Maximum**: This saves the maximum value of the variable. **Minimum**: This saves the minimum value of the variable.

## Other data

![](/en/_custom/img/generic_numvars_others.jpg)

* **Analogic variable**: If the variable is an analogic signal, you can define the type of signal, the precision, the zero and the full scale to map the signal at the value it represents.
* **Counter**: If the variable is a counter, you can specify the maximum value of the counter so that PowerStudio bears it in mind if the counter reaches its limit and is reset.
* **Modbus group**: This tells whether it is necessary to read certain modbus variables as a group.

## Save and test

![](/en/_custom/img/generic_numvars_test.jpg)

* **Save:** This tells if you want to save the values of this variable in the PowerStudio database.
* **Test**: If you have properly configured the “modbus testing parameters” section described above, by choosing the icon it will display the value of the variable you have configured to check that the programming is correct.