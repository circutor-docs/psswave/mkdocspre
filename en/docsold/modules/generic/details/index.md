# General information

The basic values of a driver can be modified in the **GENERAL** tab.

![](/en/_custom/img/generic_tab_general_main.jpg)

In the **options** section, the default values are valid for the majority of devices on the market except for those in which the handbook states otherwise.

In the **parameters** section of modbus tests, you can configure the parameters of the gateway connected to the modbus equipment in order to test the values programmed.