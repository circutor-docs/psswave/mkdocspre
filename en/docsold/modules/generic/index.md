# Generic drivers

The generic drivers module allows you to create, import or modify drivers for PowerStudio. With this tool, you can:

- [X] Add new Circutor drivers without the need to update the PowerStudio version.
- [X] Modify Circutor or non-Circutor drivers by adding only the variables you need to your system.
- [X] Create your own drivers from non-Circutor equipment with a structured display in the PowerStudio client.

## List of drivers

The main screen of generic drivers shows a list of the drivers available: the official Circutor ones , those of other manufacturers and personalised ones.

![](/en/_custom/img/generic_list.jpg)

You can see the following information on this screen:

- **Name and description**: The name and description of the driver. Here you can distinguish an official Circutor driver from an external or personalised one. The names of official drivers are in bold, accompanied by a star icon. These drivers cannot be either modified or deleted.

- **Unpublished**: If a driver is marked as “Unpublished”, it means that it is not available in the **editor** until it is published. Use the “Publish” action.

- **Actions**: The actions available for each driver. Official Circutor drivers cannot be either deleted or modified. External or personalised drivers can be both deleed and modified.

Plus, from the toolbar you can create new drivers, import drivers and publish or delete them en masse.