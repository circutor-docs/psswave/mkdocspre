# Stand-bs

PowerStudio Wave events allow a stand-by to be defined in the activation and deactivation. These stand-bys can be expressions that are checked starting at the moment the event is activated and when it ceases being in force.

![](/en/_custom/img/events_delay_modal.jpg)

## Activation stand-by

This value corresponds to the **activation stand-by** of the event in seconds. The event will be activated when the activation condition is fulfilled **at least** for the length of time obtained by calculating the expression entered in this field when the event becomes activated.

## Deactivation stand-by

This value corresponds to the **deactivation stand-by** of the event in seconds. The event will be deactivated when, once activated, the activation condition ceases to be fulfilled **at least** for the length of time indicated according to the expression in this field (the time will be calculated starting when the event ceases being in force) similar to the activation stand-by.