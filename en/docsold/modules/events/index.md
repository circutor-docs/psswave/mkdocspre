# Events

The events module allows actions in PowerStudio to be configured. Through this module, you can programme any action according to the status of any variable in the system.

Create alarms that notify you of any anomalous variable via email or telegram notifications.

Programme the connection or disconnection of any circuit according to the time, day or value of any variable.

In short, the events module is what allows you to programme the control actions of your SCADA in PowerStudio.