# Filters

Here, all you have to do is choose the device from the list of “**Elements Available**” and send it to the list on the right (“Elements Chosen”). Once there, if you double-click on the device chosen or on the gear, a window will open where you can choose the variables of that device.

![](/en/_custom/img/dataexport_filters.jpg)

On this screen, you should send the variables you want to export to the right. If you already started the export when configuring the database, it will start as soon as you click on the “Apply” button.