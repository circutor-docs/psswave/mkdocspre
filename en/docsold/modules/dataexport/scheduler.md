# Data export programmer

The programmer module enables you to export historical data from the engine prior to the installation of PowerStudio. That is, if you have historical values from years prior to this version, you can create export programming by telling the engine what period you want to export to your new database.

![](/en/_custom/img/dataexport_scheduler_list.jpg)

## Configuring export programming

If you click on “New”, a pop-up window will appear with a configuration tool.

First set the interval of dates that you want in the **initial date** and **end date** fields, and to avoid saturating the engine, during what **times** of the **days** you want that operation to be run, given that it can create a heavy workload depending on the data to be exported.

![](/en/_custom/img/dataexport_scheduler_modal.jpg)

Finally, you can configure the IP address and engine port if you don’t want the default ones, and your credentials, if you have them.