# Configuration

In the **“Database”** section, you can find its configuration, where you have to fill in the Server username and password in order to connect to the equipment's database environment and create the new export database.

!!! note “Note”

    Dado que se trata de la base de datos propietaria del usuario hasta que no se cumplimente la “cadena de conexión” no será posible dicha exportación.

![](/en/_custom/img/dataexport_setup.jpg)

In the **server** field, you should add the **“Server Name”** of SQLServer that the user has installed in their machine and provide the **username** and **password** in their respective fields.

This activates the “Start export” button; if it is activated in this step in the database, the information from the engine will arrive.

![](/en/_custom/img/dataexport_start_button.jpg)

The next step is to establish the “Filters”, which are what data of what variables you want to export.