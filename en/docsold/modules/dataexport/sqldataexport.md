# SQLDataExport vs PSSDataExport

## SQLDataExport - Structure

SqlDataExport is a programme which is installed separately from PowerStudio and is programmed to import the historical data of certain variables after a certain data or for a given period of time.

Its database structure is as follows:

![](/en/_custom/img/sqldataexport_schema1.jpg)

It primarily contains configuration tables (Engines, Devices, Variables) which contains the entity parameters (name, description, URL, port, etc.) and the parameters of the download programme.

It also contains the tables of values like the “**std_wat_values**”, which contains the standard values (STD and Watt).

There are also other tables of values like the device's Events, the user's Events and Device closures.

## PSSDataExport - Structure

PSSDataExport is the service included in PowerStudio Wave. Once it is configured for all devices and their variables, or for a selection defined by the user, it will download the historical data in **real time** to a new SQL server database determined by the user.

![](/en/_custom/img/sqldataexport_schema2.jpg)

The main data tables are those that contain the values for the engines, devices and each of the variables. Their standard data are in the “**StandardValues**” table.

Similarly yet more comprehensively, there are tables for historical data on quality events, device events and user events, as well as the fundamental harmonics and their decompositions. If the application contains devices with closures, their values will be in the “**InvoiceClosureValues**” table.

Another difference compared to the previous SQLDataExport support programme is that in the PSSDataExport microservice, the historical values of the **calculated groups** and their variables have their own tables distinct from the tables on the device variables.

![](/en/_custom/img/sqldataexport_schema3.jpg)