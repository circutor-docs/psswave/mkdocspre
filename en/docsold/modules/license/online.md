# Online activation

To license PowerStudio, you need to have a user license. Check the [Plans](/es/home/license/) section to acquire a license.

With your license, you’ll have:

* **License ID**
* **License password**

## Activation

Activating a PowerStudio license online is the quickest and easiest way to activate your license.

To do so, follow these steps:

### 1\. Activate online

Click on the “Activation online” action.

![](/en/_custom/img/licenses_online_activation_button.jpg)

### 2\. License information

Enter your **license ID** and your **license password**. Additionally, you can **name** your license in order to more easily identify it.

![](/en/_custom/img/licenses_online_modal.jpg)

### 3\. Activate the license

Click on “Accept” to activate your license.

## Deactivation

To deactivate your license, you can click on the “Deactivate” action.

![](/en/_custom/img/licenses_deactivation_button.jpg)

## Refresh license

Licenses activated online can be refreshed with the “Refresh” action to update their status.

![](/en/_custom/img/licenses_refresh_button.jpg)