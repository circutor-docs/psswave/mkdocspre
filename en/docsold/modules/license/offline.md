# Activation without connection

To license PowerStudio, you need to have a user license. Check the [Plans](/es/home/license/) section to acquire a license.

With your license, you’ll have:

* **License ID**
* **License password**

## Activation

Activating a PowerStudio license without connection enables PowerStudio to be activated on a computer without an Internet connection; however, it requires you to have another terminal with an Internet connection, as the activation code of the license provided will need to be retrieved in order to validate it.

To do so, follow these steps:

### 1\. Activate without connection

Click on the “Activation without connection” action

![](/en/_custom/img/licenses_offline_activation_button.jpg)

### 2\. License information

Enter your **license ID** and your **license password**. Additionally, you can **name** your license in order to more easily identify it.

![](/en/_custom/img/licenses_offline_modal.jpg)

### 3\. Query string

Click on the “Generate query string” button. This will generate a text string that you have to copy.

![](/en/_custom/img/licenses_offline_modal_generate_button.jpg)

### 4\. Terminal with connection

From another terminal with an Internet connection, you should access the supplier’s license activation screen. Enter the following address in a browser:

`https://secure.softwarekey.com/solo/customers/ManualRequest.aspx`

### 5\. Validate license

On the license activation page, enter the query string generated in step 3 and click on “Submit”.

![](/en/_custom/img/licenses_softwarekey_1.jpg)

### 6\. Activation string

The page will give you an activation code. Copy the activation code and paste it in the **Activation string** text field of the PowerStudio Wave activation screen.

![](/en/_custom/img/licenses_softwarekey_2.jpg)

### 7\. Activate the license

Click on “Accept” to activate your license.

## Deactivation

To deactivate your license, you can click on the “Deactivate” action.

![](/en/_custom/img/licenses_deactivation_button.jpg)