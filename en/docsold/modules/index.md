# Introduction to modules

**PowerStudio Wave** has modules that enable you to configure the new PowerStudio functionalities as well as different aspects of the application.

Here you’ll find all the information you need to use the modules. Discover all the power of PowerStudio Wave in this section.