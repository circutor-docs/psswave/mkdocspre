# Configuration of the engine

In this section, you can personalise the properties of the PowerStudio engine, similar to the way you previously did with the PSSEngineManager application. This section is divided into three parts: Engine, Authentication and File route.

![](/en/_custom/img/settings_engine_main.jpg)

## Engine

The “Engine” section shows the name of the engine, and you have the option of configuring:

* **IP port**: This indicates the port that the engine will use to initiate the web server. Requests from the editor and client will be handled in this port.
* **Default time zone**: This defines the time on which the Engine will work and can be different to the system’s time zone.

## Authentication

In this “Authentication” section, you can configure the username and password if you want the engine to have the editing authentication activated. If you do not need editing authentication, you can leave these fields blank.

## File route

In the “File route” section, you can configure the following routes:

* **Data**: Working directory to store the data downloaded from the equipment.
* **Configuration**: Working directory to store the application’s configuration.
* **Images**: Working directory to store the application’s images.

***

Once you have changed the information you wanted to change, click on the “Save” button to apply the changes. If at any point you want to go back to the previously configured values while modifying the information, click on the “Reset” button.