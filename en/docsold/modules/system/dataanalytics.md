# Data use

PowerStudio Wave collects use data to improve the user experience. The data collected are exclusively on the use of the software, information on how it is used and potential errors that may arise. These data contain no personal or sensitive information.

## Use policy

These data shall not be shared with third parties and will be exclusively for Circutor's analytical use. By accepting this policy, the user agrees to allow the software to collect and use these data in order to improve the user experience.

If you do not want to participate in the collection and use of data, you can choose to deactivate this option.

## Deactivating data use

You can deactivate the option of sharing data from the side window that is accessible from the licenses screen. To deactivate this option, unmark the option “I agree to the collection of data use”.

![](/en/_custom/img/dataanalytics_main.jpg)