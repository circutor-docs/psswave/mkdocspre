# Services

The services screen monitors the services needed for PowerStudio Wave, both Internet Information Services (IIS) and engine services (Windows Services).

## IIS Services

IIS Services show a list of all the IIS services that PowerStudio needs. Each of these services shows is status.

- **Initiated**: The service is initiated and operating without problems.
- **Degraded**: The service is initiated but has a function that is degraded.
- **Delayed**: The service is delayed.
- **Deteriorated**: The service cannot be initiated due to some problem and should be checked.

Each service has actions to initiate or delay it from PowerStudio Wave without the need to go to the IIS services management. Furthermore, the “Info” icon shows additional information on each service, such as the state of the disc, the memory and the related databases.

![](/en/_custom/img/services_microservices.jpg)

## Engines

The engine services show a list of the engines installed as Windows services. Just like the IIS Services, each engine shows its status:

- **Initiated**: The service is initiated and operating without problems.
- **Degraded**: The service is initiated but has a function that is degraded.
- **Delayed**: The service is delayed.
- **Deteriorated**: The service cannot be initiated due to some problem and should be checked.

Just like before, you can delay or initiate any engine service from PowerStudio Wave.

![](/en/_custom/img/services_engines.jpg)