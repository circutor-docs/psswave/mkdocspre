# PowerStudio Wave

Discover the main new features of the new web interface for PowerStudio.

## A new user experience.

PowerStudio’s new **Wave** tool (Display and Editing web app) is an innovative user interface that has new functionalities that enable you to interact with PowerStudio more easily, quickly and comfortably.

Wave provides an improved user experience with intuitive browsing and a visually appealing interface that makes it easier to configure and edit your application. Now you can get the most power from PowerStudio from any web browser.

Wave offers a variety of modules that will enable you to maximise their utility:

* Creation of categorised **drivers**.
* Design of personalised **dashboards**.
* Configuration of **events/alarms** with a visual, intuitive interface.
* Real time **data export** of variables logged on SQL servers.
* Integration of the **Engine Manager** tool into the new Wave interface.
* Monitoring of **services** and system logs.

<hr />
    
Si quieres saber más sobre cada funcionalidad, dirígete a [Módulos](/es/modules/).
