# Plans

In this section you’ll find complete information on PowerStudio plans and accessories and how to buy a Circutor software license.

## Versions

To efficiently manage any type of project, PowerStudio offers **three versions** which adapt to your business depending on your needs. They all have all the PowerStudio elements, such as the Editor, the Engine, the Client and the new tool, Wave, with all the functionalities outlined in the [Modules](/es/modules/) section.

Choose the best version for you according to the equipment you need to manage:

* PowerStudio Scada **Basic** up to 25 devices.
* PowerStudio Scada **Pro** up to 50 devices.
* PowerStudio Scada **Ultimate** up to 500 devices.
* PowerStudio Scada **Enterprise** more than 500 devices.

All the plans are compatible with Circutor equipment or any brand with Modbus communications.

![](/en/_custom/img/plans2.jpg)

If you need to expand your license in the future, this process will be quick and easy, with no complications.

## How to acquire your license

If you want to acquire your PowerStudio license, you can contact your usual purchase channel. If this is your first contact with Circutor, please send your request to **iot@circutor.com** or fill out the following form:

[Request form :octicons-arrow-right-24:](https://circutor.com/contacto-iot/){:target="_blank" .md-button .md-button--primary}

We’ll be happy to talk with you and help you find the solution that best meets your needs.

## OPC-UA Server accessory

You can turn your PowerStudio into a OPC-UA data server thanks to this data connector. If you want to work with your own Scada OPC, choose what Power Studio variables you want to publish in the OPC server and launch the inquiries you require in real time. Analyse the energy efficiency of your building wherever and whenever you want.

![](/en/_custom/img/plans_opc.jpg)