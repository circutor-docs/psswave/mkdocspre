# Event-oriented architecture

PowerStudio Wave is based on a new distributed architecture of **microservices** and is **geared at events**.

This new architecture eliminates the scalability restrictions of the previous versions of PowerStudio (4.x), helping to ensure that the functions run more quickly and efficiently, especially when many users are using it at the same time.

Plus, this new internal design allows new functionalities to be added and any problems detected to be corrected with just minimal effects on the product's overall operation, thus facilitating development work.

In short, the new architecture of **PowerStudio Wave** allows the product to grow and better match clients’ needs.

## Technical details

The different functionalities are implemented in **microservices**, or services specialised in a single task which are run in a distributed fashion and exchange information with each other in the form of events.

Plus, each microservice only responds to the specific events needed to implement its functionality.

The infrastructure needed to run PowerStudio Wave has only increased minimally. This is important ito bear in mind, especially because an asynchronous messaging system is needed that allows the microservices to communicate with each other: a messaging 'broker’, also called an 'event bus’. **RabbitMQ** is the one chosen.

## Architecture diagram

![](/en/_custom/img/architecture_eda.jpg)

## Most important microservices

All the functionalities of PowerStudio 4.x are still available in **PowerStudio Wave**, some of them already implemented in dedicated microservices and others still waiting to be migrated. Plus, new functionalities in new microservices have been added.

### Functionalities of PSS4.x

- [ ] PowerStudio editor
- [ ] JAVA PowerStudio client
- [ ] HTML5 PowerStudio client
- [ ] PowerStudio engine

***

### Functionalities in Microservices

- [X] PowerStudio Wave
- [X] Data exporter
- [X] Exporting programme
- [X] Events
- [X] Monitor
- [X] Generic drivers
- [X] Licensing
- [X] Data analysis