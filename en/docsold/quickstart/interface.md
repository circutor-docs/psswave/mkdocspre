# User interface

## Introduction

The main screen of PowerStudio Wave is divided into **four** spaces: **Heading**, **Browsing**, **Content** and **Page bottom**.

![](/en/_custom/img/interface_screen.jpg)

## Heading

On the upper right side of the heading you can find the options to change the language and close the session. You can also see the name of the user who started the session.

![](/en/_custom/img/interface_header.jpg)

### Language change

PowerStudio Wave is currently available in three languages: Spanish, English and Catalan. To change the language, just open the drop-down menu and choose the language you want.

### Close session

On the upper right corner after the name of the user who initiated the session, you will see the **close session** icon. By clicking on this icon, the session will close and the **session start** screen will automatically appear.

## Browsing

On the left side of the screen is the browser with all the modules and functionalities of PowerStudio Wave. Each of them is outlined in the [modules](/es/modules/) in this documentation.

![](/en/_custom/img/interface_navigation.jpg)

As the functionalities of PowerStudio Wave are expanded, the browsing will be expanded and adapted to provide access to all the functionalities in an easy, orderly fashion.

## Page bottom

On the lower right side of the screen is the page bottom. In the page bottom, you can check “About”, activate the generic help and hide/show the browsing and the heading in order to have the content on the full screen.

![](/en/_custom/img/interface_footer.jpg)

### Full screen

The full screen icon hides/shows the browsing and heading in order to get more space for the content and work more comfortably.

Plus, if you click on the F11 key, the browser becomes full screen and you can have even more room for the content.