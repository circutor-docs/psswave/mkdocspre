# Installation guide

## Installation

Even though the installation wizard will guide you through the installation process, this guide aims to resolve some of the simple steps by providing further information.

## Security message

Some of the antiviruses may detect the installation file as an unknown application. In these cases, a warning message will appear. The application should nonetheless be run in order to begin the installation process.

![](/en/_custom/img/install_warning.jpg)

## Welcome

Before starting the installation, the wizard will ask for certain information needed for the installation. Once you have provided the required information, the installation will begin.

![](/en/_custom/img/install_01.jpg)

To move forward in the installation process, click on the “**Next**” button.

## Licence agreement

You have to accept the software licence agreement to continue the installation.

![](/en/_custom/img/install_02.jpg)

## Prerequisites

PowerStudio needs certain prerequisites in the system to operate. The installer will check and install these requirements before installing PowerStudio. Any requirements that are already there do not need to be installed.

The necessary requirements are:

- [X] Java Runtime Environment SE 1.8.0
- [X] Microsoft Visual C++ 2017 Redistributable
- [X] Microsoft Visual C++ 2019 Redistributable
- [X] Windows Installer 3.1 Redistributable

## Client information

For the installation, you have to enter the data on the user, organisation and serial number. This information is irrelevant for the software, but it is important if you want to define and distinguish different PowerStudio installations.

![](/en/_custom/img/install_03.jpg)

## Type of installation

The installer enables you to make a complete installation or a personalised installation to install only certain parts of the software. We always recommend doing the **COMPLETE** installation.

![](/en/_custom/img/install_04.jpg)

## Personalised ports

The services needed will be installed in certain ports by default. Some of them cannot be changed, but others can be personalised if the proposed ones are in use. The personalisable ports are the ports for the **Wave**, **Identity** and **Administrator** services.

To learn about the functionality of each service, you can check the “Microservices” section of this documentation.

![](/en/_custom/img/install_05.jpg)

## Initiating the installation

After providing the information needed, the installation is ready to start.

Click on the "**Start**” button to start the installation.

![](/en/_custom/img/install_06.jpg)

## Installing

The installation starts and the entire PowerStudio package is installed.

![](/en/_custom/img/install_07.jpg)

## Finishing

When the installation is complete, the completed installation screen is shown. When clicking on “Finish”, the installer will close and PowerStudio will be installed in the system.

![](/en/_custom/img/install_08.jpg)

After the installation, the system may need to be rebooted. If so, a reboot message will be displayed. We recommend always rebooting after running PowerStudio for the first time.