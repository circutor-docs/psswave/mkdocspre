# Downloading PowerStudio

**PowerStudio Wave** is an application within the **PowerStudio** package that allows CIRCUTOR integration to be managed from the browser in an easy, user-friendly way. To begin to use **PowerStudio Wave**, you have to download and install the PowerStudio application package.

## Downloading

You can download the latest version of PowerStudio from here.

[Download the latest version: 24.0.0 :octicons-arrow-right-24:](https://circutor.com/productos/software/software-local/){ .md-button .md-button--primary}

## Minimal requirements

* Processor: **10th generation** **i5** (equivalent or superior)
* RAM memory: **16 Gb**
* Storage: **2 Tb**
* Operating system: **Windows Server 2016** (or superior) / **Windows 10 Pro** (or superior)
* **Ethernet adaptor**

## Do you have a previous version installed?

If you have a previous version of PowerStudio installed, you have to **deinstall** the previous version **BEFORE** installing the new version.

!!! quote "Note"

    Todas las configuraciones de la versión anterior no se perderán y estarán disponibles en la nueva instalación.

Once any previous version of PowerStudio has been deinstalled, you can install the new version.

## Initiating the installation

Once you have downloaded the software, you should decompress the ZIP file and start the installation wizard by running the compressed file.

```title="Nombre del archivo"
PowerStudio_Setup_24.x.x.x_x64.exe
```

!!! quote "Note"

    La versión y la fecha están incluida en el nombre del archivo. Recomendamos comprobar que se va a instalar la última versión de PowerStudio. Puedes consultar la última versión en el [registro de cambios](/es/changelog/).