# Access

The installation of PowerStudio includes the installation of a series of applications and all the architecture needed for the software to operate. PowerStudio Wave is one of the applications installed.

## Access to PowerStudio Wave

You can access PowerStudio Wave in two ways:

=== "From the start menu"

    ![](/en/_custom/img/access_startmenu.jpg){ align=left width=250 }
    
    A través del menú de inicio de Windows, se puede acceder a **PowerStudio Wave** ejecutando el acceso directo que se ha creado en la carpeta Circutor.
    
    !!! tip "Consejo"
    
        Puedes crear un **acceso directo** para abrir directamente PowerStudio Wave en tu navegador predeterminado.

=== "From the browser"

    También se puede acceder a PowerStudio Wave directamente a través del navegador que prefieras. Para esto, accede a la URL de localhost desde tu navegador, accediendo por el puerto configurado en la instalación.
    
    Introduce en la barra de direcciones del navegador la siguiente ruta:
    
        https://localhost:8091
    
    Por defecto, el puerto de PowerStudio Wave es el ==**8091**==. Si has cambiado el puerto durante la instalación, deberás acceder a la URL por el puerto que has especificado durante la instalación.
    
    ??? quote "¿Cómo sé si he instalado PowerStudio Wave en el puerto por defecto o lo he cambiado durante la instalación?"
    
        Para saber el puerto en el que se ha instalado el servicio de PowerStudio Wave, puedes acceder a PowerStudio Wave desde el acceso directo del menú inicio. De esta manera, en dirección del navegador podrás ver el puerto donde se ha instalado el servicio.
    
        ``` 
        https://localhost:8892 
        ```
    
        En este ejemplo, el puerto del servicio de PowerStudio Wave es el **8892**.

---

## The connection is not private

Every time you use your web browser to access a site, it checks the presence of an SSL certificate. Today's browsers need these certificates to ensure the integrity and security of the data exchanged with websites.

In the case of PowerStudio Wave, a self-signed certificate is used. Even though this software is run locally and does not require a conventional SSL certificate, it is important to bear in mind that some browsers may show a warning about the privacy of the connection due to the absence of a standard SSL certificate.

Below you can see the warning and the steps in Chrome, Edge and Firefox.

=== "Google Chrome"

    ![](/en/_custom/img/access_chrome_ssl.jpg)
    
    Para continuar, es necesario completar el acceso a localhost.
    
    * Hacer click en el botón "**Configuración avanzada**"
    * Hacer click en el enlace "**Acceder a localhost (no seguro)**"

=== "Microsoft Edge"

    ![](/en/_custom/img/access_edge_ssl.jpg)
    
    Para continuar, es necesario completar el acceso a localhost.
    
    * Hacer click en el botón "**Avanzado**"
    * Hacer click en el enlace "**Continuar a localhost (no seguro)**"

=== "Mozilla Firefox"

    ![](/en/_custom/img/access_firefox_ssl.jpg)
    
    Para continuar, es necesario completar el acceso a localhost.
    
    * Hacer click en el botón "**Avanzado...**"
    * Hacer click en el enlace "**Aceptar el riesgo y continuar**"

!!! note "Note"

    Esta configuración se recordará en el navegador y no se volverá a preguntar.

---

## Start session

![](/en/_custom/img/login.jpg)

PowerStudio Wave manages two types of users, an administrator (**admin**), with all the privileges activated, and a guest user (**user**), with limited privileges.

For the first connection, you have to **connect with the administrator and activate the product**.

The default credentials of both users are:

=== "Administrator"

    ```
    Usuario: admin
    Contraseña: admin1234
    ```

=== "Guest"

    ```
    Usuario: user
    Contraseña: user1234
    ```

!!! tip "Tip"

    Es recomendable cambiar las contraseñas de los usuarios "admin" y "user". Consulta la sección "[Usuarios](/settings/users)" para más información

---

## Activation

!!! example "Demo mode"

    Mientras el producto no esté activado, sólo dispondrás de una versión de demostración de **60 minutos**.

### Acquiring a license

If you do not have a product license, you should contact Circutor to acquire a license. Check the "[Plans](/es/home/license)" section to learn out the options for acquiring a license.

### Activating the product

If you already have a product license, you should activate PowerStudio from the license management. Check the module on "[Licences](/es/modules/license/online)" to find out the activation options of PowerStudio.