---

hide:

- navigation
- toc
- feedback
- footer

---

# PowerStudio documentation

<div class='card-container'>
<a class='card' href='/en/home/'>
<img src='/en/_custom/img/index_powerstudio.jpg' />
<span class='title'>PowerStudio</span>
<span class='content'>Descubre qué es PowerStudio, la nueva herramienta Wave y la arquitectura del software.</span>
</a>
<a class='card' href='/en/quickstart/download/'>
<img src='/en/_custom/img/index_launch.jpg' />
<span class='title'>Cómo empezar</span>
<span class='content'>Da tus primeros pasos en PowerStudio de forma ágil y sencilla.</span>
</a>
<a class='card' href='/en/modules/'>
<img src='/en/_custom/img/index_modules.jpg' />
<span class='title'>Módulos</span>
<span class='content'>Aprende cómo configurar y exprimir al máximo cada funcionalidad de PowerStudio Wave.</span>
</a>
</div>
