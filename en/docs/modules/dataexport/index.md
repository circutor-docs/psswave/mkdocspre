---
hide:
  - feedback
---
# Data export

The data export module enables you to export the data from the PowerStudio database to a SQL server database. The Microsoft SQL Server version must be 2008 or higher.

Through this module, the data will be inserted into the SQL database as they are being saved in PowerStudio.

You can also programme an export of historical data from the PowerStudio database to SQL.

In order for this module to work properly, you have to [Configure](setup.md) the database (user's proprietary database, not provided by the software) and define [Filters](filters.md) to export the values of the variables from the devices.