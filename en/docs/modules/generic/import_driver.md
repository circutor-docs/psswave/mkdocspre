# Import an official Circutor driver

To add a new Circutor driver, access the device from the Circutor product page and download the driver.

[Circutor Drivers :octicons-arrow-right-24:](https://circutor.com/){ .md-button .md-button--primary}

Enter generic Drivers from PowerStudio WAVE and click on Import on the toolbar.

![](/en/_custom/img/generic_import_button.jpg)

A dialogue box will open, where you can choose the file downloaded from the Circutor page. Once you have chosen the file, the driver will be added to the list (marked as official driver) and **it will have to be published** with the “Publish” action in order to make it available in the editor.

![](/en/_custom/img/generic_unpublished_detail.jpg)