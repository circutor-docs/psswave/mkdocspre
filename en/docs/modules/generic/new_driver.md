# Create a new driver

If you have equipment which are not Circutor, you can **create your own driver**. The first thing you need is the manufacturer’s **memory map** for the equipment, which specifies the address of the variable as well as its features.

In the example below, you can see the modbus map of a photovoltaic inverter. The inverter’s handbook provides the following information:

![](/en/_custom/img/generic_map_demo.jpg)

After entering all your data from the memory map, all you have to do is **save** and **publish** in order to use the driver in PowerStudio.

<hr />
Check the details of each driver editing section in the [Details of a driver](/en/modules/generic/details/) section.