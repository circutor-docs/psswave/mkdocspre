# Modify a driver

If you want to maintain the original driver, you can make a copy with the “Duplicate” action. To modify an existing driver, click on the name of the driver or on the “Edit” action, and a driver editing screen will open.

![](/en/_custom/img/generic_edit_main.jpg)

On this screen, you can edit the name and description fo the diver. Plus, there are 4 sections to modify the driver:

* **General**: In this section, you can modify the basic data of the driver.
* **Numerical variables**: List of numerical variables. These are the variables that can have any numerical value.
* **Binary variables**: List of binary variables. These are the varaibles can can only have two values: 0 or 1.
* **Images**: The images which are used to show the driver in the editor.

## List of variables

On the variables tabs, you can see the list of the driver's variables and modify them individually. Additionally, you can delete a group of variables en masse by choosing them and using the “Delete n rows” action.

![](/en/_custom/img/generic_edit_vars.jpg)

<hr />
Check the details of each driver editing section in the [Details of a driver](/en/modules/generic/details) section.