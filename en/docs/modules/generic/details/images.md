# Images

Here you can add images from the device that will later be displayed in the PowerStudio SCADA client and editor. This section only affects the user experience, so it is optional (if no images are added, PowerStudio Wave will load the generic driver image by default).

![](/en/_custom/img/generic_tab_images_main.jpg)

You can choose the file with the image of the device with the “Import” buttons.

To use it in the PowerStudio editor and client, the images have to have the size in pixels specified for each option: small at **16px x 16px**, medium at **50px x 50px** and large at **100px x 100px**.