# Event conditions

When creating or modifying an event condition, a form appears where you should indicate the identifier, the engine on which the condition will be run and the expression.

!!! note "Note"

    When showing the condition of an event in the flowchart, you can choose whether the text of the identifier or the expression is shown. You can activate and deactivate the “Show identifier” button on the same form.

![](/en/_custom/img/events_conditional_modal.jpg)

Variables of devices registered in PowerStudio and logical operators can be added in the **“Expression”** field, such as greater than or equal to… (>=), lower than… (<) equal to… (=) and others. The device variables are easy to identify because they are defined between brackets.

You can also add all sorts of functions to create expressions and conditions. When choosing the functions that you want to use, a table appears on the right side which explains how to use that function and the parameters needed for it to work.