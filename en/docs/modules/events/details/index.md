# Events

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctoe massa, nec semper lorem quam in massa.

## Events

When accessing events through the browser menu, the list of available events appears. To create a new event, click on “New”.

![](/en/_custom/img/events_create_new_button.jpg)

Then fill out the form with the event identifier or name. You can also add one or several tags which you can later use to filter a search. Finally, to save the event, click on “Save”.

A flowchart with all the possible options for creating a new event will be displayed by default. This new event will be on **stand-by** by default, and will remain so until it is activated using the “Play” button that appears on the upper right of the event diagram display. An event can be initiated/placed on stand-by at any time.

![](/en/_custom/img/events_create_diagram_new_stopped.jpg)