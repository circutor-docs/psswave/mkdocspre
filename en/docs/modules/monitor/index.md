---
hide:
  - feedback
---
# Monitor

The Monitor module allows you to create dashboards with widgets to show the information on your installation with an attractive, intuitive display.

The widgets incorporated into WAVE enable you to easily generate modern displays.