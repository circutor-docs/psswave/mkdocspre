# Monitor configuration

**PowerStudio Wave** includes a tool to create dashboards: the **Monitor**. This new functionality improves user experience by providing an intuitive, real-time display of the data and status of your installation.

To configure this tool, go to the submenu Monitor configuration.

## General

In this tab, you can define the main features of the Monitor display:

* Show designs on the monitor
* Activate the **rotation** of designs
* Configure the rotation time
* Configure the data **refresh time**
* Configure titles, colours and background images
* Block the configuration of designs in the display

![](/en/_custom/img/monitor_main.jpg)

## Images

This tab contains the PowerStudio Wave monitor image manager.

The manager enables you to **store** the images you want to use in both Monitor configuration and in the Widgets and to **organise them in folders**. Plus, you can **search** inside each folder.

![](/en/_custom/img/monitor_images.jpg)

## Real-time data

This tab shows all the real-time data of all the variables belonging to a widget.

![](/en/_custom/img/monitor_realtime.jpg)

## Administration

This tab enables you to import and export monitor configurations. Specifically, exports generate a **JSON** file which enables the entire configuration of the monitor to be replicated by importing in another PowerStudio WAVE monitor.

![](/en/_custom/img/monitor_admin.jpg)

## Widgets

PowerStudio Scada WAVE enables you to show any information you want in the format you prefer.

![](/en/_custom/img/monitor_widgets_screen.jpg)

### Designs

The “Add” button creates a new layout.

Beside it is the “Clone” button, which is very useful for replicating the active layout and modifying it as needed.

Different layouts or screens can be created, in which the widgets are entered in the form and size you prefer.

![](/en/_custom/img/monitor_layout_modal.jpg)

### Available widgets

![](/en/_custom/img/monitor_widgets_list.jpg)

The different widgets you can choose from are:

* Graphic widget
* Current power
* Single value
* Circular progress
* Image widget
* Image carousel
* Html widget
* Rss widget
* Gauge widget
* Group of widgets
* Personalisable widget

!!! example "In development"

    This tool is currently in the development phase and the designs made may not be compatible with future versions of PowerStudio WAVE.

### Inserting widgets

To insert each widget, simply drag it from the side menu to the layout.

![](/en/_custom/img/monitor_widgets_drag.jpg)

### Configuration of widgets

Once you have inserted each widget, you can choose its size using the size arrows.

![](/en/_custom/img/monitor_widgets_resize.jpg)

By clicking on the **Gear**, you enter the widget personalisation menu.

From this menu, you can choose the variables to represent in the widget, as well as the graphics, colour, shading, images and other parameters.

![](/en/_custom/img/monitor_widgets_settings.jpg)

### Variables 

To enter variables in each of the widgets, you can choose among the variables available in your device tree configured in the Editor.

![](/en/_custom/img/monitor_widgets_varlist.jpg)

You can copy the public html iframe code of each of the widgets to embed it in the website you wish.

!!! warning "Important"

    Remember to click on the Record button to save the changes!