---
hide:
  - navigation
  - toc
  - feedback
  - footer
---
# PowerStudio documentation

<div class='card-container'>
<a class='card' href='/en/home/'>
<img src='/en/_custom/img/index_powerstudio.jpg' />
<span class='title'>PowerStudio</span>
<span class='content'>Learn all about PowerStudio, the new Wave tool and the software architecture.</span>
</a>
<a class='card' href='/en/quickstart/download/'>
<img src='/en/_custom/img/index_launch.jpg' />
<span class='title'>Getting started</span>
<span class='content'>Get started in PowerStudio quickly and easily.</span>
</a>
<a class='card' href='/en/modules/'>
<img src='/en/_custom/img/index_modules.jpg' />
<span class='title'>Modules</span>
<span class='content'>Learn how to configure and make the most of each functionality of PowerStudio Wave.</span>
</a>
</div>
