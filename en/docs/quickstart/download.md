# Downloading PowerStudio

**PowerStudio Wave** is an application within the **PowerStudio** package that allows CIRCUTOR integration to be managed from the browser in an easy, user-friendly way. To begin to use **PowerStudio Wave**, you have to download and install the PowerStudio application package.

## Downloading

You can download the latest version of PowerStudio from here.

[Download the latest version: 24.7.1 :octicons-arrow-right-24:](https://docs.circutor.com/docs/PowerStudio_Setup_24.7.1.0_241114_816_x64.zip){ .md-button .md-button--primary}

## Minimum requirements

* Processor: **10th generation** **i5** (equivalent or superior)
* RAM memory: **16 Gb**
* Storage: **2 Tb**
* Operating system: **Windows Server** (2016 or superior) / **Windows Pro** (10 or superior)
* **Ethernet adaptor**

## Do you have a previous version installed?

If you have a previous version of PowerStudio installed, you have to **uninstall** the previous version **BEFORE** installing the new version.

!!! quote "Note"

    All the settings from the previous version will be saved and available in the new installation.

Once any previous version of PowerStudio has been uninstalled, you can install the new version.

## Initiating the installation

Once you have downloaded the software, you should decompress the ZIP file and start the installation wizard by running the compressed file.

```title="File name"
PowerStudio_Setup_24.x.x.x_x64.exe
```

!!! quote "Note"

    The version and date are included in the file name. We recommend checking that the latest version of PowerStudio is installed. You can check the latest version in the [changelog](/en/changelog/).