# Access

The installation of PowerStudio includes the installation of a series of applications and all the architecture needed for the software to operate. PowerStudio Wave is one of the applications installed.

## Access to PowerStudio Wave

You can access PowerStudio Wave in two ways:

=== "From the start menu"

    ![](/en/_custom/img/access_startmenu.jpg){ align=left width=250 }
    
    You can access **PowerStudio Wave** through the Windows start menu by executing the direct access created in the Circutor folder.
    
    !!! tip "Tip"
    
        You can create a **direct access** to open PowerStudio Wave directly in your predetermined browser.

=== "From the browser"

    You can also access PowerStudio Wave directly through your favourite browser. To do so, go to the URL of the localhost from your browser via the gateway configured in the installation.
    
    Enter the following address in the browser address bar:
    
        https://localhost:8091
    
    PowerStudio Wave's default gateway is ==**8091**==. If you have changed the gateway during the installation, you should go to the URL via the gateway you have specified during the installation.
    
    ??? quote "How do I know whether I have installed PowerStudio Wave in the default gateway or whether I changed it during the installation?"
    
        To know in which gateway the PowerStudio Wave service is installed, you can access PowerStudio Wave in the direct access on the start menu. In the browser address there, you will be able to see the gateway where the service was installed.
    
        ``` 
        https://localhost:8892 
        ```
    
        In this example, PowerStudio Wave's service gateway is **8892**.

----

## The connection is not private

Every time you use your web browser to access a site, it checks the presence of an SSL certificate. Today's browsers need these certificates to ensure the integrity and security of the data exchanged with websites.

In the case of PowerStudio Wave, a self-signed certificate is used. Even though this software is run locally and does not require a conventional SSL certificate, it is important to bear in mind that some browsers may show a warning about the privacy of the connection due to the absence of a standard SSL certificate.

Below you can see the warning and the steps in Chrome, Edge and Firefox.

=== "Google Chrome"

    ![](/en/_custom/img/access_chrome_ssl.jpg)
    
    To continue, you have to complete the access to localhost.
    
    * Click on the "**Advanced settings**” button.
    * Click on the "**Access localhost (not secure)**” link

=== "Microsoft Edge"

    ![](/en/_custom/img/access_edge_ssl.jpg)
    
    To continue you have to complete the access to localhost.
    
    * Click on the "**Advanced**” button.
    * Click on the "**Continue to localhost (not secure)**” link

=== "Mozilla Firefox"

    ![](/en/_custom/img/access_firefox_ssl.jpg)
    
    To continue you have to complete the access to localhost.
    
    * Click on the "**Advanced**” button.
    * Click on the "**Accept the risk and continue**” link

!!! note "Note"

    The browser will remember this configuration and will not ask again.

----

## Start session

![](/en/_custom/img/login.jpg)

PowerStudio Wave manages two types of users, an administrator (**admin**), with all the privileges activated, and a guest user (**user**), with limited privileges.

For the first connection, you have to **connect with the administrator and activate the product**.

The default credentials of both users are:

=== "Administrator"

    ```
    User: admin
    Password: admin1234
    ```

=== "Guest"

    ```
    User: user
    Password: user1234
    ```

!!! tip "Tip"

    We recommend changing the “admin” and “user” passwords. Check the "[Users](/settings/users)” section for more information.

----

## Activation

!!! example "Demo mode"

    As long as the product is not activated, you will only have a **60-minute** demonstration version.

### Acquiring a license

If you do not have a product license, you should contact Circutor to acquire a license. Check the "[Plans](/en/home/license)" section to learn out the options for acquiring a license.

### Activating the product

If you already have a product license, you should activate PowerStudio from the license management. Check the module on "[Licences](/en/modules/license/online)" to find out the activation options of PowerStudio.