---
hide:
  - feedback
---
# Change log

## 24.7.1 <small><small>- 09/11/2024</small></small> 

* Improved performance of the PowerStudio service.
* Improved performance of the microservices.

## 24.7.0 <small><small>- 09/11/2024</small></small> 

* Zones now support generic drivers.
* Improved performance of the Monitor module.
* Generic drivers now support channels.
* Date format updated to ISO8601.
* Embedded version for Line-EDS compatible with B100 and B150.

## 24.6.2 <small><small>- 15/10/2024</small></small> 

* Performance improvements.
* Fixed time zone management in embedded systems.
* Fixed errors when exporting applications without screens or reports.

## 24.6.1 <small><small>- 11/10/2024</small></small> 

* Fixed errors in Windows when the engine loses communication.
* Fixed unsaved calculated variables.

## 24.6.0 <small><small>- 07/10/2024</small></small> 

* Improved CPU performance of virtual drivers.
* Enhanced export time for large applications.
* New enabled/disabled event indicator in the Editor's event list.
* New functionality for importing events from the Editor to Wave.
* New functionality to export and import events in Wave.
* Event display in Wave in table format.
* Interface improvements in Wave.
* Fixed license limit errors.
* Fixed errors in a customized Editor installation.
* Minor fixes.

## 24.5.6 <small><small>- 10/09/2024</small></small> 

* Fixed display of the device tree with generic drivers.

## 24.5.5 <small><small>- 06/09/2024</small></small> 

* Fixed errors with generic drivers without images.

## 24.5.4 <small><small>- 31/07/2024</small></small> 

* Fixed issues with authentication and HTTPS communication in the engine.

## 24.5.3 <small><small>- 25/07/2024</small></small> 

* Improved download time for applications with lower-level engines in the editor.
* Fixed issue with importing or exporting an application in the editor, which didn't work correctly when using username/password.
* Embedded version for Line-EDS compatible with Line-M-4G module.

## 24.5.2 <small><small>- 15/06/2024</small></small> 

* Minor fixes.

## 24.5.1 <small><small>- 08/06/2024</small></small> 

* Fixed bugs identified in QNA412.

## 24.5.0 <small><small>- 05/06/2024</small></small> 

* Improved top-level template management.
* Fixed errors in the Administrator service.
* Begin and End variables for generic drivers.

## 24.4.2 <small><small>- 10/05/2024</small></small>

* Security improvements for Windows server environments.
* Permanent deactivation of microservices is now allowed.

## 24.3.0 <small><small>- 18/04/2024</small></small>

* CVM C11 Ethernet is added.
* Calculated variables are always published on the event bus.

## 24.2.1 <small><small>- 30/03/2024</small></small>

* Enhancement in service status identification.
* Minor fixes.

## 24.2.0 <small><small>- 11/03/2024</small></small>

* Embedded variables are added to the WAVE tool.

## 24.1.5 <small><small>- 22/02/2024</small></small>

* Improvements in memory management of microservices.

## 24.1.4 <small><small>- 13/02/2024</small></small>

* Bug fixes in the PSS Admin service.

## 24.1.3 <small><small>- 12/02/2024</small></small>

* Improvements in the management of the SQL database.

## 24.1.2 <small><small>- 09/02/2024</small></small>

* Improvements in the management of https communications.
* Minor fixes.

## 24.1.1 <small><small>- 08/02/2024</small></small>

* Improvements to the Monitor tool.

## 24.1.0 <small><small>- 06/02/2024</small></small>

* Improvements in the optimisation of SQL database tables.

## 24.0.1 <small><small>- 03/02/2024</small></small>

* Improvements in event bus communication.

## 24.0.0 <small><small>- 01/02/2024</small></small>

* :party_popper: :party_popper: Launch of PowerStudio Wave!