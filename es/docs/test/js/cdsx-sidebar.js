checkSidebarType();

function checkSidebarType() {
    if (localStorage.getItem("sidebarType") === null)
        localStorage.setItem("sidebarType", "expanded-sidebar");

    if (localStorage.getItem("sidebarType") === "expanded-sidebar") {
        document.body.classList.remove('collapsed-sidebar');
        document.body.classList.add('expanded-sidebar');
    } else {
        document.body.classList.remove('expanded-sidebar');
        document.body.classList.add('collapsed-sidebar');
    }
}

function toggleSidebarType() {

   //document.querySelectorAll('.navItemOpen').forEach(element => element.classList.remove('navItemOpen'));

    if (localStorage.getItem("sidebarType") === null)
        localStorage.setItem("sidebarType", "expanded-sidebar");

    if (localStorage.getItem("sidebarType") === "expanded-sidebar") {
        localStorage.setItem("sidebarType", "collapsed-sidebar");
    } else {
        localStorage.setItem("sidebarType", "expanded-sidebar");
    }
    checkSidebarType();
}

checkSidebarBehavior();

function checkSidebarBehavior() {
    if (localStorage.getItem("sidebarBehavior") === null)
        localStorage.setItem("sidebarBehavior", "shown-sidebar");

    if (localStorage.getItem("sidebarBehavior") === "shown-sidebar") {
        document.body.classList.remove('hidden-sidebar');
        document.body.classList.add('shown-sidebar');
    } else {
        document.body.classList.remove('shown-sidebar');
        document.body.classList.add('hidden-sidebar');
    }
}

function toggleSidebarBehavior() {
    if (localStorage.getItem("sidebarBehavior") === null)
        localStorage.setItem("sidebarBehavior", "shown-sidebar");

    if (localStorage.getItem("sidebarBehavior") === "shown-sidebar") {
        localStorage.setItem("sidebarBehavior", "hidden-sidebar");
    } else {
        localStorage.setItem("sidebarBehavior", "shown-sidebar");
    }
    checkSidebarBehavior();
}