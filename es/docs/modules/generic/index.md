---
hide:
  - feedback
---
# Controladores

El módulo de controladores permite crear, importar o modificar controladores para Powerstudio. Mediante esta herramienta puedes: 

- [x] Añadir nuevos controladores de Circutor sin necesidad de actualizar la versión de PowerStudio. 
- [x] Modificar los controladores de Cirutor o no Circutor, añadiendo a tu sistema solo las variables que necesites.
- [x] Crear tus propios controladores de equipos no Circutor, con una visualización estructurada en el cliente de PowerStudio. 

## Lista de controladores

En la pantalla principal de controladores genéricos se muestra una lista de los controladores disponibles, tanto los oficiales de Circutor como los de otros fabricantes y los personalizados.

![](/es/_custom/img/generic_list.jpg)

En esta pantalla puedes ver los siguientes datos:

- **Nombre y descripción**: El nombre y la descripción del controlador. Aquí puedes diferenciar un controlador oficial Circutor de uno externo o personalizado. Los controladores oficiales tienen el nombre en negrita y van acompañados de un icono de estrella. Estos controladores no se pueden modificar ni eliminar.

- **No publicado**: Si un controlador tiene la marca de "No publicado" significa que no está disponible en el **editor** hasta que se publique. Usa la acción de ++"Publicar"++.

- **Acciones**: Las acciones disponibles para cada controlador. Los controladores oficiales de Circutor no se pueden Eliminar ni modificar. Los controladores externos o personalizados sí se pueden eliminar y modificar.

Además, desde la barra de herramientas puedes crear controladores nuevos, importar controladores y publicar o eliminar masivamente.