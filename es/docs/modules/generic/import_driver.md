# Importar un controlador oficial de Circutor

Para añadir un nuevo controlador de Circutor, accede al dispositivo desde la página de productos de Circutor y descarga el controlador. 

[Controladores Circutor :octicons-arrow-right-24:](https://circutor.com/){ .md-button .md-button--primary}

Entra en Controladores desde PowerStudio WAVE y haz click en Importar desde la barra de herramientas.

![](/es/_custom/img/generic_import_button.jpg)

Se abrirá un cuadro de dialogo en el que podrás seleccionar el archivo descargado de la página web de Circutor.
Una vez seleccionado el archivo se añadirá el controlador a la lista (marcado como controlador oficial) y **será necesario publicarlo** para que esté disponible en el editor con la acción de ++"Publicar"++.

![](/es/_custom/img/generic_unpublished_detail.jpg)