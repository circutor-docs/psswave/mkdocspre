# Crear un nuevo controlador

En el caso de tener equipos que no sean Circutor puedes **crear tu propio controlador**. Lo primero que necesitas es el **mapa de memoria** del fabricante del equipo, donde se especifica la dirección de la variable, así como sus características.

En el siguiente ejemplo podemos ver el mapa modbus de un inversor fotovoltaico. En el manual del inversor nos indica la siguiente información: 

![](/es/_custom/img/generic_map_demo.jpg)

Después de introducir todos los datos del mapa de memória, solo es necesario **guardar** y **publicar** para poder disponer del controlador el PowerStudio. 

<hr />

Consulta los detalles de cada apartado de edición de un controlador en la sección [Detalles de un controlador](/es/modules/generic/details/).