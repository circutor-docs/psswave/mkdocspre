# Variables binárias

En la pestaña **VARIABLES BINÁRIAS** podremos introducir todas las variables binárias del mapa modbus que deseemos.

![](/es/_custom/img/generic_tab_binvars_main.jpg)

Si seleccionamos la acción ++"Nueva variable"++, se abre un cuadro de dialogo que permite configurarla.

## General

![](/es/_custom/img/generic_binvars_general.jpg)

* **Identificador**: Nombre con el que se quiere identificar la variable.
* **Nombre**: Nombre de la variable.
* **Descripción**: Información adicional a la variable (opcional).
* **Tipo**: Tipo de variable. Puede ser lectura o escritura (información del mapa modbus del fabricante).
* **Código de función de lectura**: Código de función para la lectura de la variable (información del mapa modbus del fabricante) 
* **Código de función de escritura**: Código de función para la escritura de la variable (información del mapa modbus del fabricante). 

## Metadatos

![](/es/_custom/img/generic_binvars_metadata.jpg)

Los metadatos indican información sobre la variable, para que PowerStudio pueda categorizarla:

* **Familia**: indica que tipo de medida estamos realizando (en caso de no estar disponible en el desplegable puede indicar uno específico). 
* **Orden**: indica el orden de la variable (disponible para variables de harmónicos). 

## Guardar y probar

![](/es/_custom/img/generic_binvars_test.jpg)

* **Guardar**: Indica si queremos guardar los valores de esta variable en la base de datos de PowerStudio.
* **Probar**: Si hemos configurado correctamente el apartado “parámetros de pruebas modbus” descrito anteriormente, seleccionando el icono, nos mostrara el valor de la variable que hemos configurado, para verificar que la programación es correcta.