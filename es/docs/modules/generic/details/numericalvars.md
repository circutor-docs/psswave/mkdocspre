# Variables numéricas

En la pestaña **VARIABLES NUMÉRICAS** podremos introducir todas las variables numéricas del mapa modbus que deseemos.

![](/es/_custom/img/generic_tab_numvars_main.jpg)

Si seleccionamos la acción ++"Nueva variables"++, se abre un cuadro de dialogo que permite configurarla.

## General

![](/es/_custom/img/generic_numvars_general.jpg)

* **Identificador**: Nombre con el que se quiere identificar la variable.
* **Nombre**: Nombre de la variable.
* **Descripción**: Información adicional a la variable (opcional).
* **Tipo**: Tipo de variable. Puede ser lectura o escritura (información del mapa modbus del fabricante).
* **Código de función de lectura**: Código de función para la lectura de la variable (información del mapa modbus del fabricante) 
* **Código de función de escritura**: Código de función para la escritura de la variable (información del mapa modbus del fabricante). 
* **Información de forzado**: información adicional que se mostrará al forzar la variable (por ejemplo, valores permitidos para la variable).
* **Dirección**: Dirección de memoria de la variable (información del mapa modbus del fabricante).
* **Registros**: número de registros de la variable: (información del mapa modbus del fabricante).

## Formato y unidades

![](/es/_custom/img/generic_numvars_format.jpg)

* **Decimales**: número de decimales con los que se muestra la variable (información del mapa modbus del fabricante). En nuestro caso es 3, ya que el fabricante indica un factor de 0,001.
* **IEE754**: Marcar cuando la variable sea de tipo Float (información del mapa modbus del fabricante).
* **Mod10**: Formato de variable modbus utilizado por algunos fabricantes. (información del mapa modbus del fabricante).
* **Con signo**: Indica si la variable tiene signo (información del mapa modbus del fabricante).
* **Unidades**: Indica las unidades de la variable (información del mapa modbus del fabricante). En caso que la unidad no esté en el desplegable, seleccionar Definido por el usuario y introducirla manualmente. 

## Criterio de agrupación

Es el criterio con el que se agrupan los datos: 

* **Valor medio**: Se guardará el valor medio de los valores que se reciban. 
* **Último valor**: Se guardará el ultimo valor que se reciba. 
* **Diferencial**: Se guardará el valor como el incremental del periodo.

!!! quote "Nota"

    Al crear una variable de tipo diferencial, PowerStudio genera automáticamente la variable de inicio y fin del periodo. Estas variables se generan con un identificador igual al de la variable diferencial más una B (inicio del periodo) y una E (final del periodo).

!!! example "Ejemplo"

    Al crear una variable de energía con identificador **AE**, se crearán las siguientes variables:
    
    * **AEB** con el valor absoluto del contador al inicio del periodo.
    * **AEE** con el valor absoluto del contador al final del periodo.
     
## Metadatos

![](/es/_custom/img/generic_numvars_metadata.jpg)

Los metadatos indican información sobre la variable, para que PowerStudio pueda categorizarla:

* **Familia**: indica que tipo de medida estamos realizando (en caso de no estar disponible en el desplegable puede indicar uno específico). 
* **Orden**: indica el orden de la variable (disponible para variables de harmónicos). 
* **Fase**: indica la fase a la que pertenece la variable. 
* **Tipo**: es el tipo de variable y puede ser: **Instantáneo**: es una variable de tipo promedio. **Máximo**: guarda el máximo de la variable. **Mínimo**: guarda el valor mínimo de la variable.

## Otros datos

![](/es/_custom/img/generic_numvars_others.jpg)

* **Variable analógica**: En el caso de que la variable sea una señal analógica, podemos definir el tipo de señal, la precisión, el cero y el fondo de escala para mapear la señal al valor que representa.
* **Contador**: en caso de que la variable se un contador, podemos especificar el valor máximo del contador para que PowerStudio lo tenga en cuenta si el contador llega a su límite y se resetea.
* **Grupo modbus**: indica si es necesario leer algunas variables modbus como un grupo.

## Guardar y probar

![](/es/_custom/img/generic_numvars_test.jpg)

* **Guardar**: Indica si queremos guardar los valores de esta variable en la base de datos de PowerStudio.
* **Probar**: Si hemos configurado correctamente el apartado “parámetros de pruebas modbus” descrito anteriormente, seleccionando el icono, nos mostrara el valor de la variable que hemos configurado, para verificar que la programación es correcta.