# Información general

En la pestaña **GENERAL** se pueden modificar los valores básicos de un controlador.

![](/es/_custom/img/generic_tab_general_main.jpg)

En el apartado **opciones**, los valores por defecto son válidos para la mayoría de equipos del mercado, salvo en aquellos en los que el manual del equipo indique lo contrario 

En el apartado **parámetros** de pruebas modbus se puede configurar los parámetros de la pasarela conectada al equipo modbus, para poder testear los valores programados. 