# Imágenes

Aquí podemos añadir las imágenes del equipo que luego visualizaremos en el cliente y editor de PowerStudio SCADA. Este apartado solo afecta a la experiencia de usuario por lo que es opcional (en caso de no añadir imágenes, PoweStudio Wave cargara la imagen del controlador por defecto).

![](/es/_custom/img/generic_tab_images_main.jpg)

Mediante los botones ++"importar"++ podemos seleccionar el archivo con la imagen del dispositivo.  

Para su utilización en el editor y cliente de PowerStudio es necesario que las imágenes tengan el tamaño en pixeles especificado para cada opción: pequeña de **16px x 16px**, mediana de **50px x 50px** y grande, de **100px x 100px**. 