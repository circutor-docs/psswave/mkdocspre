# Modificar un controlador

Si se desea mantener el controlador original, puedes realizar una copia mediante la acción ++"Duplicar"++. Para modificar un controlador existente haz click el nombre del controlador o en la acción ++"Editar"++ y se abrirá la pantalla de edición del controlador. 

![](/es/_custom/img/generic_edit_main.jpg)

En esta pantalla puedes editar el nombre y la descripción del controlador. Además, dispones de 4 apartados para modificar el controlador:

* **General**: En este apartado puedes modificar los datos básicos del controlador.
* **Variables númericas**: Listados de las variables númericas. Son aquellas variables que pueden tener cualquier valor numérico.
* **Variables binárias**: Listado de las variables binarias. Son aquellas variables que solo pueden tener dos valores: 0 o 1.
* **Imágenes**: Las imágenes que se usaran para mostrar el controlador en el editor.

## Listado de variables

En las pestañas de variables puedes ver el listado de variables del controlador y modificarlas de forma individual. Adicionalmente, puedes eliminar de forma masiva un grupo de variables seleccionandolas y usando la acción ++"Eliminar n filas"++.

![](/es/_custom/img/generic_edit_vars.jpg)

<hr />

Consulta los detalles de cada apartado de edición de un controlador en la sección [Detalles de un controlador](/es/modules/generic/details).