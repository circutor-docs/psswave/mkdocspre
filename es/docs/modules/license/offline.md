# Activación sin conexión

Para licenciar PowerStudio es necesario disponer de una licencia de usuario. Consulta la sección [Planes](/es/home/license/) para adquirir una licencia.

Con tu licencia dispondrás de:

* **ID de licencia**
* **Password de licencia**

## Activación

Activar una licencia de PowerStudio sin conexión permite activar un PowerStudio en un ordenador sin conexión a internet, no obstante requiere que dispongas de otro terminal con conexión a internet, ya que será necesario recuperar un código de activación del proveedor de licencias para validarla.

Para ello, sigue los siguientes pasos:

### 1. Activar sin conexión
Haz click en la acción de ++"Activación sin conexión"++

![](/es/_custom/img/licenses_offline_activation_button.jpg)

### 2. Datos de licencia
Introduce tu **ID de licencia** y tu **Password de licencia**. Adicionalmente puedes poner un **nombre** a tu licencia para identificarla más fácilmente.

![](/es/_custom/img/licenses_offline_modal.jpg)

### 3. Cadena de petición
Haz click en el botón ++"Generar cadena de petición"++. Se generará una cadena de texto que deberás copiar.

![](/es/_custom/img/licenses_offline_modal_generate_button.jpg)

### 4. Terminal con conexión
Desde un otro terminal con conexión a internet debes acceder a la página de activación de licencias del proveedor. Introduce en un navegador la siguiente dirección:

```https://secure.softwarekey.com/solo/customers/ManualRequest.aspx```

### 5. Validar licencia
En la página de activación de licencias, introduce la cadena de petición generada en el paso 3 y haz click en ++"Submit"++.

![](/es/_custom/img/licenses_softwarekey_1.jpg)

### 6. Cadena de activación
La página te devolverá un código de activación. Copia el código de activación y pégalo en el campo de texto **Cadena de activación** de la pantalla de activación de PowerStudio Wave.

![](/es/_custom/img/licenses_softwarekey_2.jpg)

### 7. Activar licencia
Haz click en ++"Aceptar"++ para activar tu licencia.

## Desactivación

Para desactivar una licencia puedes hacer click en la acción de ++"Desactivar"++.

![](/es/_custom/img/licenses_deactivation_button.jpg)