# Activación en línea

Para licenciar PowerStudio es necesario disponer de una licencia de usuario. Consulta la sección [Planes](/es/home/license/) para adquirir una licencia.

Con tu licencia dispondrás de:

* **ID de licencia**
* **Password de licencia**

!!! warning "Importante"
    
    ¡Recuerda que si se realiza una activación en línea el ordenador/servidor donde está instalado PowerStudio SCADA siempre debe tener conexión a internet!

## Activación

Activar una licencia de PowerStudio en línea es la manera más rápida y cómoda de activar tu licencia.

Las licencias online necesitan conexión a internet para validarse. En el caso de que el servidor disponga de acceso restringido a internet, es necesario permitir el acceso a la URL **https://secure.softwarekey.com** a través de los puertos **50786** y **443**.

Para ello, sigue los siguientes pasos:

### 1. Activar en línea
Haz click en la acción de ++"activación en línea"++.

![](/es/_custom/img/licenses_online_activation_button.jpg)

### 2. Datos de licencia
Introduce tu **ID de licencia** y tu **Password de licencia**. Adicionalmente puedes poner un **nombre** a tu licencia para identificarla más fácilmente.

![](/es/_custom/img/licenses_online_modal.jpg)

### 3. Activar licencia
Haz click en ++"Aceptar"++ para activar tu licencia.

## Desactivación

Para desactivar una licencia puedes hacer click en la acción de ++"Desactivar"++.

![](/es/_custom/img/licenses_deactivation_button.jpg)

## Refrescar licencia

Las licencias activadas en línea se pueden refrescar con la acción ++"Refrescar"++ para actualizar su estado.

![](/es/_custom/img/licenses_refresh_button.jpg)