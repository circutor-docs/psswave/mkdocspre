# Servicios

La pantalla de servicios monitoriza los servicios necesarios de PowerStudio Wave, tanto los servicios de Internet Information Services (IIS) como los servicios de motor (Servicios de Windows).

## Servicios IIS

Los servicios IIS muestran una lista de todos los servicios IIS que necesita PowerStudio. Cada uno de estos servicios muestra su estado.

- **Iniciado**: El servicio está iniciado y funcionando sin problemas.
- **Degradado**: El servicio está iniciado pero tiene alguna función degradada.
- **Detenido**: El servicio está detenido.
- **Deteriorado**: El servicio no se puede iniciar por algún problema y se debe revisar.

Cada servicio dispone de acciones para iniciarlo o detenerlo desde PowerStudio Wave sin necesidad de ir a la gestión de servicios de IIS. Además, el icono de "info" muestra información adicional de cada servicios, como el estado del disco, la memoria y las bases de datos relacionadas.

![](/es/_custom/img/services_microservices.jpg)

## Motores

Los servicios de motor muestran una lista de los motores instalados como servicios de Windows. De la misma manera que los Servicios IIS, cada motor muestra su estado:

- **Iniciado**: El servicio está iniciado y funcionando sin problemas.
- **Degradado**: El servicio está iniciado pero tiene alguna función degradada.
- **Detenido**: El servicio está detenido.
- **Deteriorado**: El servicio no se puede iniciar por algún problema y se debe revisar.

Como antes, desde PowerStudio Wave se puede detener o iniciar cualquier servicio de motor.

![](/es/_custom/img/services_engines.jpg)