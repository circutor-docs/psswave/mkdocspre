# Uso de los datos

PowerStudio Wave recopila datos de uso para mejorar la experiencia del usuario. Los datos recopilados son exclusivamente sobre el uso del software, información sobre cómo se utiliza y posibles errores que puedan surgir. Estos datos no incluyen información personal o datos sensibles.

## Política de uso

Estos datos no serán compartido con terceros y serán de uso analítico exclusivo de Circutor. Al aceptar esta política, el usuario acepta que el software recopile y use estos datos de uso para mejorar la experiencia del usuario.

Si no deseas participar en la recopilación y el uso de datos, puedes optar por deshabilitar está opción.

## Deshabilitar el uso de los datos

Se puede deshabilitar la opción de compartir los datos desde la ventana lateral accesible desde la pantalla de licencias. para deshabilitar la opción se debe desmarcar la opción de "Acepto la recopilación del uso de los datos".

![](/es/_custom/img/dataanalytics_main.jpg)