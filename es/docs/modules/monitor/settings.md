# Configuración del monitor

**PowerStudio Wave** incluye una herramienta para la creación de dashboards, el **Monitor**. Esta nueva funcionalidad mejora la experiencia de usuario al proporcionar una visualización intuitiva y en tiempo real de los datos y el estado de tu instalación.

Para parametrizar esta herramienta deberemos acceder al submenú de Configuración del monitor.

<iframe width="100%" height="400" src="https://www.youtube.com/embed/cBKjw5apAWk?si=u6v-W7wcvGhenmVp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

## General

En esta pestaña puedes definir las características principales de la visualización del Monitor:

* Mostrar diseños en el monitor
* Activar la **rotación** de diseños
* Configurar el tiempo de rotación
* Configurar el **tiempo de refresco** de datos
* Configurar títulos, colores e imágenes de fondo
* Bloquear la configuración de diseños en el visualizador

![](/es/_custom/img/monitor_main.jpg)

## Imágenes

En esta pestaña se encuentra el gestor de imágenes del monitor de PowerStudio Wave.

El gestor permite **almacenar** las imágenes que desees utilizar tanto en la Configuración del monitor como en los propios Widgets y **organizarlas en carpetas**. Además, puedes **buscar** en el interior de cada una de las carpetas.

![](/es/_custom/img/monitor_images.jpg)

## Datos en tiempo real

En esta pestaña se muestran los datos de en tiempo real de todas aquellas variables que pertenecen a algún Widget.

![](/es/_custom/img/monitor_realtime.jpg)

## Administración

Esta pestaña te permite importar y exportar configuraciones del monitor. Concretamente la exportación genera un archivo **JSON** que permite replicar la configuración entera del monitor haciendo una importación en otro monitor de PowerStudio WAVE.

![](/es/_custom/img/monitor_admin.jpg)

## Widgets

PowerStudio Scada WAVE permite mostrar la información que desees, en el formato que más te guste.

![](/es/_custom/img/monitor_widgets_screen.jpg)

### Diseños

El botón ++"Añadir"++, nos crea un Layout nuevo.

A su lado, tenemos el botón ++"Clonar"++, muy útil para replicar el layout activo y modificarlo según sea necesario.

Se pueden crear diversos Layouts o pantallas, en las que se introducen los widgets de la forma y tamaño que prefieras.

![](/es/_custom/img/monitor_layout_modal.jpg)

### Widgets disponibles

![](/es/_custom/img/monitor_widgets_list.jpg)

Los distintos widgets entre los que puedes elegir son:

* Widget de gráfico
* Potencia actual
* Single value
* Progreso circular
* Widget de imagen
* Caroussel de imágenes
* Widget html
* Widget rss
* Widget gauge
* Grupo de widgets
* Widget personalizable

!!! example "En desarrollo"

    Esta herramienta está en fase de desarrollo y los diseños hechos pueden no ser compatibles con versiones futuras de PowerStudio WAVE.

### Insertar widgets

Para insertar cada widget, simplemente se arrastra desde el menú lateral hasta el Layout.

![](/es/_custom/img/monitor_widgets_drag.jpg)

### Configuración de widgets

Una vez has insertado cada Widget, puedes elegir su tamaño usando las flechas de dimensión.

![](/es/_custom/img/monitor_widgets_resize.jpg)

Pulsando sobre el **Engranaje**, se entra en el menú de personalización de Widget.

Desde este menú puedes seleccionar las variables a representar en el widget, así como los parámetros de gráfico, colores, sombreados, imágenes, etc.

![](/es/_custom/img/monitor_widgets_settings.jpg)

### Variables

Para introducir variables en cada uno de los widgets, puedes elegir de entre las variables disponibles en tu árbol de dispositivos, configurado en el Editor.

![](/es/_custom/img/monitor_widgets_varlist.jpg)

Puedes copiar el código iframe html público de cada uno de los widgets para incrustarlo en la web que desees.

!!! warning "Importante"

    ¡Recuerda pulsar el botón Grabar para guardar los cambios!






