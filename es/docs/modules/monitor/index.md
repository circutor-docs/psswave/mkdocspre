---
hide:
  - feedback
---
# Monitor

El módulo Monitor permite crear dashboards con widgets para mostrar la información de tu instalación con una visualización atractiva e intuitiva. 

Los widgets incorporados en WAVE, permiten generar modernas visualizaciónes de manera sencilla.  