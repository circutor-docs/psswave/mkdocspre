# Programador de la exportación de datos

El módulo programador nos permite exportar datos históricos del motor anteriores a la instalación del PowerStudio, es decir si tenemos valores históricos de años anteriores a esta versión, podemos crear programaciones de exportaciones indicándole al motor que periodo queremos exportar a nuestra nueva base de datos. 

![](/es/_custom/img/dataexport_scheduler_list.jpg)

## Configuración de una programación de exportación

Si se hace click en ++"Nuevo"++ aparecerá una ventana emergente donde aparecerá una herramienta de configuración. 

Primero estableceremos el intervalo de los datos que queremos con los campos **fecha inicial** y **fecha final** y, para no saturar al motor, durante que **horas** de los **días** queremos que se lleve a cabo dicha operación dado que, en función de los datos a exportar, puede tratarse de trabajos muy costosos. 

![](/es/_custom/img/dataexport_scheduler_modal.jpg)

Por último, podemos configurar dirección IP y puerto del motor, si no fuesen los por defecto y sus credenciales, si las tuviera. 