# SQLDataExport vs PSSDataExport

## SQLDataExport - Estructura

SqlDataExport  se trata de un programa que se instala por separado de PowerStudio que se programa para que importe los datos históricos de ciertas variables a partir de una fecha o para un periodo determinado de tiempo. 

Su estructura de Base de Datos es la siguiente:

![](/es/_custom/img/sqldataexport_schema1.jpg)

Principalmente consta de tablas de configuración (Engines, Devices, Variables) donde aparte de los parámetros de la entidad (nombre, descripción, uri, puerto...) también contienen los parámetros de la programación de la descarga. 

Por otra parte, están las tablas de valores como la “**std_wat_values**” que contiene los valores standard (STD y Watt). 

También existen otras tablas de valores como las de Eventos del dispositivo, de Sucesos del usuario y de Cierres de dispositivos. 

## PSSDataExport - Estructura

PSSDataExport es el servicio incluido en PowerStudio Wave, que una vez configurado para todos los dispositivos y sus variables o para una selección definida por el usuario, descargará los datos históricos en **tiempo real** a una nueva base de datos sqlserver que determine el usuario. 

![](/es/_custom/img/sqldataexport_schema2.jpg)

Las principales tablas de datos son las que contienen los valores para los motores, dispositivos y para cada una de las variables, sus datos standard están en la tabla “**StandardValues**”. 

De forma parecida, pero más completa, existen tablas para datos históricos de eventos de calidad, eventos de dispositivos, eventos de usuario, así como los harmónicos fundamentales y sus descomposiciones. Si la aplicación contiene dispositivos que tengan cierres, tendremos sus valores en la tabla “**InvoiceClosureValues**”. 

Otra diferencia respecto al anterior programa de apoyo SQLDataExport es que en el microservicio PSSDataExport los valores históricos de los **grupos de calculadas** y sus variables tienen sus propias tablas diferenciadas de las de variables de dispositivos. 

![](/es/_custom/img/sqldataexport_schema3.jpg)