---
hide:
  - feedback
---
# Exportación de datos

El módulo de exportación de datos permite exportar los datos de la base de datos de PowerStudio a una base de datos SQL server. La versión de Microsoft SQL Server debe ser del 2008 o superior.

Mediante este módulo, los datos se insertarán en la base de datos SQL en el mismo momento que se guarden en PowerStudio.  

Tambien puedes programar una exportación de datos históricos de la base de datos de PowerStudio a SQL.

Para el correcto funcionamiento de este módulo es necesario establecer una [Configuración](setup.md) de la base de datos (propietaria del usuario, no proporcionada por el software) y definir unos [Filtros](filters.md) para exportar los valores de que variables de que dispositivos. 