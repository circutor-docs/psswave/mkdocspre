# Configuración

En la sección **“Base de Datos”** encontramos la configuración de la misma, donde se debe cumplimentar el Servidor el usuario y la contraseña, para poder conectarnos al entorno de la base de datos del equipo y crear la nueva base de datos de exportación. 

!!! note "Nota"

    Dado que se trata de la base de datos propietaria del usuario hasta que no se cumplimente la “cadena de conexión” no será posible dicha exportación.

![](/es/_custom/img/dataexport_setup.jpg)

En el campo **servidor** se deberá añadir el **“Server Name”** de SQLServer propio que tenga instalado el usuario en su máquina y aportar el **usuario** y **contraseña** en los respectivos campos. 

De esta manera se habilita el Botón ++"Iniciar exportación"++, si se habilita en este paso a la base de datos ya llegará la información del motor.

![](/es/_custom/img/dataexport_start_button.jpg)

El siguiente paso será establecer los “Filtros” que serán qué datos de que variables queremos exportar. 