# Filtros

Aquí solo hay que seleccionar el dispositivo de la lista de “**Elementos Disponibles**” y pasarlo a la lista de la derecha (“Elementos Seleccionados”), una vez aquí si hacemos doble-click en el dispositivo seleccionado o pinchamos en la rueda dentada, se nos abrirá una ventana para seleccionar las variables de dicho dispositivo. 

![](/es/_custom/img/dataexport_filters.jpg)

En esta pantalla enviaremos a la derecha las variables que queremos exportar. Si en el paso de la configuración de la base de datos ya le habíamos dado a iniciar exportación, ésta se inicia inmediatamente al pinchar en el botón ++"Aplicar"++.