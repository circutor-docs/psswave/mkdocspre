---
hide:
  - feedback
---
# Introducción a módulos

**PowerStudio Wave** dispone de módulos que te permiten configurar las nuevas funcionalidades de PowerStudio, así como configurar diferentes aspectos de la aplicación.

Aquí encontrarás la información necesaria para la utilización de los módulos. Descubre toda la potencia de PowerStudio WAVE en esta sección.