# Comunicaciones

En la sección de comunicaciones, puedes configurar las propiedades necesarias para enviar correos electrónicos y mensajes a través de Telegram. Estos servicios de mensajería se utilizan en el apartado de Sucesos.

## Servidor saliente SMTP

![](/es/_custom/img/settings_comms_smtp_main.jpg)

En la subsección de “Servidor saliente (SMTP)”, puedes configurar todos los aspectos necesarios para el envío de correos electrónicos: 

* **Dirección de correo electrónico**: Es la dirección del remitente del correo. 
* **Servidor y puerto**: Configuración de comunicación con el servidor SMTP de envío, por ejemplo, smtp.outlook365.com y puerto 587 para el servidor SMTP de Outlook. 
* **Usuario y contraseña**: las credenciales de conexión para el servidor SMTP de envío de correos. 
* **Utilizar encriptación TLS para la conexión**: Marcar esta opción es necesario si el servidor SMTP permite el uso de este tipo de encriptación. 
* **Habilitado**: Permite activar o desactivar el envío de correos electrónicos. Esta opción es útil si deseas deshabilitar el envío de correos configurados en los Sucesos en un momento dado. 

Si deseas probar la configuración de envío de correo antes de habilitarla, puedes hacer clic en el botón '**Probar**'. 

***

Una vez hayas modificado la información según tus preferencias, haz clic en el botón '**Guardar**' para aplicar los cambios. Si en algún momento, mientras realizas modificaciones, decides volver a los valores previamente configurados, presiona el botón '**Reset**'.

## Telegram

![](/es/_custom/img/settings_comms_telegram_main.jpg)

En la subsección de “Configuración Telegram” puedes ajustar lo siguiente: 

* **Nombre**: El nombre del Bot de Telegram. 
* **Identificador de Bot de Telegram**: Este identificador se utilizará internamente con la API de Telegram para enviar mensajes específicos. 
* **Habilitado**: Activa o desactiva el envío de mensajes a través de Telegram. Esta opción resulta útil, por ejemplo, si deseas desactivar temporalmente el envío de mensajes de Telegram configurados en los Sucesos. 

Si deseas realizar una prueba de la configuración antes de activarla, puedes hacer clic en el botón '**Probar**'. 

***

Después de ajustar la información deseada, haz clic en el botón '**Guardar**' para aplicar los cambios. En caso de que decidas revertir a los valores previamente configurados en algún momento durante la modificación, pulsa el botón '**Reset**'. 

## Configuración para el envío de Telegram 

Pasos a seguir para la obtener las credenciales de configuración para el envío de Telegram:

### Crear un Bot de Telegram y obtener su botId 

* Acceder al navegador y visita [https://web.telegram.org/a/ ](https://web.telegram.org/a/ )
* Utiliza la aplicación Telegram en tu dispositivo móvil. Ve a **Ajustes > Dispositivos** y selecciona "**Vincular un dispositivo**".
* Escanea el código QR obtenido en el navegador. 
* Accede al navegador y visita [https://telegram.me/botfather](https://telegram.me/botfather)
* En "**BotFather**", crea un nuevo bot utilizando el comando "**/newbot**". Introduce el nombre del bot, por ejemplo, **BOT_NAME**, y asigna el nombre de usuario para el bot, terminando en "**_bot**", por ejemplo, "**bot_name_bot**". 
* Para ver todos los bots creados, en botFather usar el comando “**/mybots**”. Para obtener el token del bot tendremos que acceder, de la lista de bots mostrados, a nuestro bot y pulsar la opción “**API Token**”. 
* Para acceder al bot creado, ir a la url **t.me/bot_username** donde **bot_username** podría ser, por ejemplo, **bot_name_bot** (t.me/bot_name_bot).
* Para iniciar el bot, pulsa el botón “**Start**” en el chat del propio bot.  
* Existen tres posibles destinatarios de mensajes de Telegram: un **usuario**, un **grupo** o un **canal**.  
* Para obtener el identificador de usuario buscamos el usuario **@userinfobot**. Accede al chat del bot y pulsa “**Start**”. Debería aparecer el identificador de tu usuario, que sería el identificador del destinatario.  

### Crear un canal Telegram para enviar mensajes con el bot

* No es posible crear el canal directamente desde Telegram Web.  
* Utiliza la aplicación móvil/PC para agregar el bot al canal como administrador con **permisos de envío de mensajes**. 
* Usa la aplicación de escritorio para facilitar la creación del canal. Asigna un nombre al canal (por ejemplo, PSS_Channel) y configúralo como **público** temporalmente para obtener el "**channel_id**" más adelante. 
* Para agregar el bot al canal, ve a la aplicación del teléfono y añádelo al canal. 
* Una vez creado el canal, envía un mensaje al canal mediante el navegador con la siguiente URL.

    
```title="URL de ejemplo"
https://api.telegram.org/bot<API_TOKEN>/sendMessage?chat_id=@powerstudiotest&text=TEST 
```

Donde **API_Token** es el identificador del bot y **“powerstudiotest”** es el nombre del canal  

* La respuesta debería devolver un código **200** y un **JSON** 
* Toma nota del campo “id” dentro de “chat”. Este valor es el “chat_id” de la petición. 
* Opcionalmente, vuelve a hacer el canal privado.  

Ahora tienes el **bot_id** y el **chat_id** necesarios para enviar mensajes a Telegram mediante la API. 

!!! note "Nota"
 
    En caso de querer crear un grupo desde Telegram Web, los pasos son los mismos que para crear un canal. 