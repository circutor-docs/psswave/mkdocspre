# Configuración del motor

En esta sección, puedes personalizar las propiedades del motor Powerstudio, de manera similar a como se hacía anteriormente con la aplicación PSSEngineManager. La sección se divide en tres partes: Motor, Autenticación y Ruta de los ficheros.

![](/es/_custom/img/settings_engine_main.jpg)

## Motor

En el apartado “Motor”, se muestra el nombre del motor, y además tienes la opción de configurar: 

* **Puerto IP**: Indica el puerto que utilizará el motor para iniciar el servidor web. En este puerto se atenderán las peticiones del editor y cliente. 
* **Zona horaria por defecto**: Define la zona horaria sobre la cual trabajará el Motor pudiendo ser distinta a la del sistema. 

## Autenticación

En el apartado “Autenticación” podrás configurar el usuario y contraseña si deseas que el motor tenga habilitada la autenticación de edición. En caso de no necesitar autenticación de edición, puedes dejar estos campos vacíos.

## Ruta de los ficheros

En el apartado “Ruta de los ficheros” puedes configurar las siguientes rutas: 

* **Datos**: Directorio de trabajo para almacenar los datos descargados de los equipos. 
* **Configuración**: Directorio de trabajo para almacenar la configuración de la aplicación. 
* **Imágenes**: Directorio de trabajo para almacenar las imágenes de la aplicación. 

***

Una vez hayas cambiado la información deseada, haz clic en el botón ++"Guardar"++ para aplicar los cambios. Si en algún momento, mientras modificas la información, decides volver a los valores previamente configurados, presiona el botón ++"Reset"++. 