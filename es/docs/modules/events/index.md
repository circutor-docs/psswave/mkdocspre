---
hide:
  - feedback
---
# Sucesos

El módulo de sucesos permite la configurar acciones en PowerStudio. Mediante este módulo, puedes programar cualquier acción en función del estado de cualquier variable del sistema.  

Crea alarmas que te informen de algún valor anómalo, notificándotelo por email o telegram. 

Programa la conexión o desconexión de cualquier circuito en función de la hora, el dia, o el valor de alguna variable. 

En definitiva, el módulo de sucesos es el que te permite programar las acciones de control de tu SCADA en PowerStudio.