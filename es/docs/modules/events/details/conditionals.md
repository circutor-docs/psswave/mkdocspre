# Condiciones de los sucesos

Al crear o modificar la condición de un suceso aparece un formulario donde debe indicar el identificador, el motor sobre el que se ejecutará la condición y la expresión.

!!! note "Nota"

    A la hora de mostrar la condición de un suceso en un diagrama de flujo podrá elegir si mostrar el texto del identificador o la expresión. Podrás habilitar y deshabilitar el botón de “Mostrar identificador” desde el mismo formulario.

![](/es/_custom/img/events_conditional_modal.jpg)

En el campo **“Expresión”** se pueden añadir variables de equipos dados de alta en PowerStudio y operadores lógicos como, por ejemplo, mayor o igual que… (>=), menor que… (<) igual que… (=), entre otros. Las variables de dispositivos son de fácil identificación ya que se encuentran definidas entre corchetes.

También puede añadir funciones de todo tipo para crear expresiones y condiciones. Al seleccionar las funciones que quiere utilizar le aparece en la parte derecha un cuadro donde se explica cómo utilizar dicha función y los parámetros necesarios para que funcione.