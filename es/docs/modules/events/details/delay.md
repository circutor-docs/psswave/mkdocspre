# Retardos

Los sucesos en PowerStudio Wave permiten definir un retardo en la activación y en la desactivación. Dichos retardos pueden ser expresiones que se verifican a partir del momento en el que el suceso actúa y a partir del momento en el que el suceso deja de estar vigente.

![](/es/_custom/img/events_delay_modal.jpg)

## Retardo en la activación

Este valor corresponde al **retardo en la activación** del suceso en segundos. El suceso será activado cuando se cumpla la condición de activación durante **al menos** el tiempo que se obtiene de calcular la expresión introducida en este campo en el momento en que el suceso pasa a estar vigente.

## Retardo en la desactivación

Este valor corresponde al **retardo en la desactivación** del suceso en segundos. El suceso será desactivado cuando, estando activado, la condición de activación se deje de cumplir **al menos** el tiempo indicado según la expresión de este campo (el cálculo del tiempo se realizará a partir del momento en el que el suceso deja de estar vigente), de forma análoga al retardo en la activación.