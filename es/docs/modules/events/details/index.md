# Sucesos 

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctoe massa, nec semper lorem quam in massa.

## Sucesos

Al acceder a sucesos a través del menú de navegación aparece el listado de sucesos disponibles. Para crear un nuevo suceso se debe hacer click en ++"Nuevo"++.

![](/es/_custom/img/events_create_new_button.jpg)

A continuación, se debe rellenar el formulario con el identificador o nombre del suceso. También puede añadir una o varias etiquetas para posteriormente filtrar una búsqueda.. Finalmente, para guardar el suceso debe hacer click en ++"Guardar"++.

Por defecto se mostrará un diagrama de flujo con todas las opciones posibles para crear un nuevo suceso. Este nuevo suceso estará **detenido** por defecto, y lo estará hasta que se active, usando el botón ++"play"++ que aparece en la parte superior derecha de la visualización del diagrama de sucesos. Se puede iniciar/detener un suceso en cualquier momento.

![](/es/_custom/img/events_create_diagram_new_stopped.jpg)