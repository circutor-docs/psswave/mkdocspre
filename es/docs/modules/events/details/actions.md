# Acciones de los sucesos

## ¿Qué son las acciones de los sucesos?

Una acción es el resultado de una ejecución de un suceso en Wave.
Esta acción se llevará a cabo cuando se cumpla o deje de cumplir un suceso según lo que haya definido un usuario.

## ¿Cómo funcionan las acciones de los sucesos en PowerStudio Wave?

Primero hay que generar una condición para que se ejecute dicha acción que definiremos más adelante.

Pongamos un ejemplo para poder entenderlo mejor.

Si la energía activa de un dispositivo es menor o igual a 0 entonces nos mandará un correo de aviso para que nos demos cuenta de que ese dispositivo no está en funcionamiento:

![](/es/_custom/img/events_action_diagram_example.jpg)

En este caso tenemos un suceso que se activará si la energía activa del dispositivo CVM-C10 queda a 0 y lo que hará es mandar un correo de alerta en este caso a la dirección iot@circutor.com

![](/es/_custom/img/events_action_detail_activation.jpg)

Si se restablece la energía del aparato y por tanto ya no tenemos el valor en 0, hemos establecido que nos mande un correo de alerta para que sepamos que vuelve a tener energía.

![](/es/_custom/img/events_action_detail_deactivation.jpg)

## Criterios de activación

Tenemos 4 posibles criterios de activación a la hora de crear un suceso:

* **Al activarse**: Esta es la acción que se ejecutará en cuanto sea cierto el suceso que hemos definido y se haya cumplido el tiempo de retraso de activación definido anteriormente.

* **Mientras está activo**: Esta es la acción que se ejecutará mientras el suceso que hemos definido sea cierto y se haya cumplido el tiempo de retraso de activación definido anteriormente.

* **Al desactivarse**: Esta es la acción que se ejecutará en cuanto el suceso que hemos definido deje de cumplirse (sea falso) y se haya cumplido el tiempo de retraso de activación definido anteriormente.

* **Mientras está inactivo**: Esta es la acción que se ejecutará mientras el suceso que hemos definido sea falso y se haya cumplido el tiempo de retraso de activación definido anteriormente.

## Tipos de acciones

Estas son las distintas acciones en los sucesos en Power Studio Wave:

![](/es/_custom/img/events_action_options_modal.jpg)

### Forzado de variable

El forzado de variable sirve para, como bien dice el nombre, forzar la ejecución de una variable de un dispositivo. En este caso por ejemplo será que si se cumple una función se abrirá el relé de un EDS (en este caso debemos saber que el estado abierto de un relé es 0 y el cerrado es 1).

![](/es/_custom/img/events_action_force_modal.jpg)

* **Identificador**: Es una herramienta que nos sirve para poder mostrar de forma más sencilla y entendible la variable forzable. Deberemos marcar la opción “Mostrar identificador” para verlo en pantalla:

* **Motor**: Es el motor donde estará, en este caso, la variable forzable.

* **Variable**: La variable que definimos y que se ejecutará cuando se cumpla o se deje de cumplir el suceso dependiendo de lo que hayamos definido previamente. Disponemos de un botón de wizard para no tener que escribir la variable de forma manual.

* **Valor**: Es donde escribimos el resultado de la variable forzable. En este caso es el valor 0 ya que nos servirá para abrir ese relé.

### Envío de correo

Gracias a esta opción podremos enviar correos desde PowerStudio Wave a los correos indicados como alarma por si por ejemplo dejamos de tener comunicación con un aparato o algún dispositivo está dando valores que no son correctos según nuestro criterio.

![](/es/_custom/img/events_action_email_modal.jpg)

!!! warning "Importante"

    Antes de empezar con esta parte es importante configurar un servidor de correo. Puedes consultar esta información en la sección de [configuración de correo](/es/modules/settings/comms).

* **Identificador**: Es una herramienta que nos sirve para poder mostrar de forma más sencilla y entendible la variable forzable. Deberemos marcar la opción “Mostrar identificador” para verlo en pantalla.

* **De**: Es el correo desde donde enviaremos ese correo y que hemos definido anterior mente en el apartado Comunicaciones .

* **Para**: El/Los correos donde vamos a mandar el corro electrónico.

!!! info "Información" 

    Separar correos por “,” y sin dejar espacios

* **Asunto**: Asunto del correo.

* **Mensaje**: El contenido del correo que recibiremos.

### Envío de Telegram

Con esta herramienta podremos mandar alertas a un chat de Telegram que nosotros queramos. Para ello deberemos crear un bot de Telegram y obtener el token de dicho bot. Para consultar la configuración de Telegram puedes consultar la sección de [configuración de Telegram](/es/modules/settings/comms).

![](/es/_custom/img/events_action_telegram_modal.jpg)

* **Identificador**: Es una herramienta que nos sirve para poder mostrar de forma más sencilla y entendible la variable forzable. Deberemos marcar la opción “Mostrar identificador” para verlo en pantalla.

* **Identificador de Bot de Telegram**: Aquí debemos escoger el Bot de Telegram creado anteriormente.

* **Mensaje**: El mensaje de aviso que llegará al chat de Telegram.