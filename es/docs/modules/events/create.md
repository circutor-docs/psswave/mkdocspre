# Cómo crear un suceso

En esta sección aprenderás a crear un suceso en PowerStudio Wave paso a paso. Además, en cada uno de los pasos, podrás acceder a toda la información detallada de cada sección.

## Paso 1: Crear un suceso

En la pantalla principal de sucesos podrás crear un nuevo suceso pulsando el botón ++"Nuevo"++ de la barra de herramientas.

![](/es/_custom/img/events_create_new_button.jpg)

## Paso 2: Datos básicos

En la pantalla de creación de sucesos, lo primero que debes hacer es darle un nombre. Además, puedes crear y añadir categorias para organizar tus sucesos y poder filtrarlos más fácilmente. Para continuar debes hacer click en el botón ++"Guardar"++.

![](/es/_custom/img/events_create_main_modal.jpg)

## Paso 3: Modo edición

Al crear un suceso, éste aparecerá en estado **"detenido"**. El diagrama de los sucesos en estado "detenido" se visualizan con una apariencia diferente. En este estado, puedes editar el diagrama del suceso. Además, las opciones de la parte superior derecha del suceso estan habilitadas (duplicar, eliminar, ...).

![](/es/_custom/img/events_create_diagram_new_stopped.jpg)

## Paso 4: Retardo

En los primeros nodos del diagrama se puede configurar el **retardo**, tanto **en la activación** como **en la desactivación** del suceso. Para ello, debes hacer click en el nodo y configurar el retardo en segundos.

![](/es/_custom/img/events_create_diagram_delay.jpg)

Puedes consultar información detallada sobre el [nodo de retardo](/es/modules/events/details/delay/).

## Paso 5: Condiciones

En los nodos de condición puedes configurar las **condiciones** que deben cumplirse para que el suceso se active. Para ello, debes hacer click en el nodo con el símbolo (+) y configurar las condiciones. Las condiciones se expresan en el editor de expresiones y se pueden usar variables y/o expresiones lógicas.

![](/es/_custom/img/events_create_diagram_conditional.jpg)

Puedes consultar información detallada sobre el [nodo de condición](/es/modules/events/details/conditionals/).

## Paso 6: Operador

En el nodo del operador puedes elegir si el operador es un ++"AND"++ o un ++"OR"++. Para ello, debes hacer click en el nodo y seleccionar el operador que se desea. El operador actuará sobre todas las condiciones previas, aplicando el operador seleccionado.

![](/es/_custom/img/events_create_diagram_operator.jpg)

## Paso 7: Acciones

Desde cada línea principal (a la activación y a la desactivación) y después de las condiciones, se pueden configurar acciones **al activarse** o **mientras está activo**. En cada una de estas ramas puedes crear o modificar las acciones que se ejecutarán en cada caso.

![](/es/_custom/img/events_create_diagram_action.jpg)

Hay tres tipos de acciones:

* Forzar una variable
* Enviar un correo electrónico
* Enviar un mensaje de Telegram

Cada una de las acciones tiene su propia pantalla de configuración.

Puedes consultar información detallada sobre las [acciones](/es/modules/events/details/actions/).

## Paso 8: Iniciar el suceso

Una vez el suceso esté configurado se debe iniciar para que se empiece a evaluar y ejecute las acciones si es necesario. Para iniciar un evento solo es necesario hacer click en el icono de ++"play"++ de las acciones del evento. Mientras un suceso esté detenido, se puede editar, duplicar o eliminar.

![](/es/_custom/img/events_create_play_button.jpg)

Un suceso iniciado se visualiza con un color diferente y no se puede editar. Para editar un suceso iniciado, primero se debe detener con el icono de ++"stop"++.

![](/es/_custom/img/events_create_stop_button.jpg)
