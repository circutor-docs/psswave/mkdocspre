# Planes

En esta sección encontrarás toda la información relacionada a los planes, los complementos a PowerStudio y cómo comprar una licencia de software Circutor.

## Versiones

Para gestionar eficientemente cualquier tipo de proyecto, PowerStudio ofrece **tres versiones** que se adaptan a tu negocio en función de tus necesidades. Todas ellas disponen de la totalidad de elementos que forman PowerStudio, como son el Editor, el Motor, el Cliente, y la nueva herramienta Wave, con todas las funcionalidades detalladas en la sección [Módulos](/es/modules/).

Escoge la versión más adecuada en función de los equipos que necesites gestionar:

* PowerStudio Scada **Basic** hasta 25 equipos.
* PowerStudio Scada **Pro** hasta 50 equipos.
* PowerStudio Scada **Ultimate** hasta 500 equipos.
* PowerStudio Scada **Enterprise** más de 500 equipos.

Todos los planes son compatibles con equipos Circutor o de cualquier marca con comunicaciones Modbus.

![](/es/_custom/img/plans2.jpg)

En caso que en un futuro requieras ampliar tu licencia, este proceso será sencillo y ágil, sin complicaciones.

## Cómo adquirir tu licencia

En caso de que desees adquirir tu licencia PowerStudio puedes contactar con tu canal de compra habitual. Si es tu primer contacto con Circutor, por favor envía tu solicitud a **iot@circutor.com** o rellena el siguiente formulario:

[Formulario de solicitud :octicons-arrow-right-24:](https://circutor.com/contacto-iot/){:target="_blank" .md-button .md-button--primary}

Estaremos encantados de atenderte y ayudarte a encontrar la solución que más se adecua a tus necesidades.

## Complemento OPC-UA Server

Puedes convertir tu PowerStudio en un servidor de datos OPC-UA gracias a este conector de datos. Si quieres trabajar con tu propio Scada OPC, escoge qué variables de PowerStudio deseas publicar en el servidor OPC y lanza las consultas que requieras a tiempo real. Analiza la eficiencia energética de tu edificio donde quieras y cuando quieras.

![](/es/_custom/img/plans_opc.jpg)