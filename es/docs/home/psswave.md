# PowerStudio Wave

Descubre las principales novedades de la nueva interfaz web para PowerStudio.

## Una nueva experiencia de usuario

La nueva herramienta **Wave** (Web App de Visualización y Edición) de PowerStudio es una innovadora interfaz de usuario que presenta nuevas funcionalidades que te permiten interactuar con PowerStudio de manera más fácil, ágil y cómoda.

Con Wave tendrás una experiencia de usuario mejorada, con una navegación intuitiva y una interfaz visualmente atractiva que simplifica la configuración y edición de tu aplicación. Ahora, ya puedes exprimir al máximo la potencia de PowerStudio desde cualquier navegador web.

Wave ofrece una variedad de módulos que te permitirán maximizar su utilidad:

* Creación de **controladores** categorizados.
* Diseño de **dashboards** personalizados.
* Configuración de **eventos/alarmas** mediante una interfaz visual e intuitiva.
* **Exportación de datos** en tiempo real de variables registradas a servidores SQL.
* Integración de la herramienta **Engine Manager** en la nueva interfaz Wave.
* Monitorización de **servicios** y logs de sistema.

<hr />
    
Si quieres saber más sobre cada funcionalidad, dirígete a [Módulos](/es/modules/).