# Arquitectura orientada a eventos

PowerStudio Wave se basa en una nueva arquitectura distribuida de **microservicios** y **orientada a eventos**. 

Esta nueva arquitectura elimina las restricciones de escalabilidad de las versiones anteriores de PowerStudio (4.x), ayudando a que las funcionalidades se ejecuten más rápido y de manera más eficiente, especialmente cuando muchos usuarios las están usando al mismo tiempo. 

Además, este nuevo diseño interno permite agregar nuevas funcionalidades y corregir problemas detectados con una mínima afectación al funcionamiento general del producto, facilitando así también el trabajo de desarrollo. 

En resumen, la nueva arquitectura de **PowerStudio Wave** permite que el producto crezca y se adapte mejor a las necesidades de los clientes. 

## Detalles técnicos

Las diferentes funcionalidades están implementadas en **microservicios**, o servicios especializados en una única tarea, que se ejecutan de manera distribuida, y que intercambian información entre ellos en forma de eventos. 

Además, cada microservicio responde solamente a los eventos específicos necesarios para implementar su funcionalidad. 

La infraestructura necesaria para poder ejecutar PowerStudio Wave se ha incrementado mínimamente. Este es un dato a tener en cuenta, en especial porque se necesita un sistema de mensajería asíncrona que permita la comunicación entre los microservicios: un ‘broker’ de mensajería, o también llamado ‘bus de eventos’. **RabbitMQ** es el elegido. 

## Esquema arquitectura

![](/es/_custom/img/architecture_eda.jpg)

## Microservicios más importantes

Todas las funcionalidades de PowerStudio 4.x siguen estando disponibles en **PowerStudio Wave**, algunas implementadas ya en microservicios dedicados, otras todavía pendientes de ser migradas. Además, se han añadido nuevas funcionalidades en nuevos microservicios.

### Funcionalidades de PSS4.x

- [ ] Editor de PowerStudio 
- [ ] Cliente JAVA de PowerStudio 
- [ ] Cliente HTML5 de PowerStudio 
- [ ] Motor de PowerStudio 

***
### Funcionalidades en Microservicios

- [x] PowerStudio Wave 
- [x] Exportador de datos
- [x] Programador de exportación
- [x] Sucesos
- [x] Monitor
- [x] Controladores
- [x] Licenciamiento
- [x] Análisis de datos