# Guía de instalación

## Instalación

Aunque el asistente de instalación te guiará durante el proceso de instalación, esta guía intenta resolver algunos paso sencillos ampliando la información.

## Mensaje de seguridad

Puede que algunos antivirus detecten el archivo de instalación como aplicación desconocida. En estos casos, aparecerá un mensaje de advertencia. Se debe ejecutar la aplicación de todos modos para comenzar el proceso de instalación.

![](/es/_custom/img/install_warning.jpg)

## Bienvenida

Antes de comenzar la instalación, el asistente pedirá cierta información necesaria para la instalación. Una vez proporcionada la información necesaria, la instalación comenzará. 

![](/es/_custom/img/install_01.jpg)

Para avanzar durante el proceso de instalación, debes hacer click en el botón "**Siguiente**".

## Acuerdo de licencia

Es necesario aceptar el acuerdo de licencia del software para continuar la instalación.

![](/es/_custom/img/install_02.jpg)

## Prerequisitos

PowerStudio necesita de ciertos prerequisitos en el sistema para funcionar. El instalador comprobará e instalará estos requisitos antes de instalar PowerStudio. Aquellos requisitos que ya se cumplan no será necesario instalarlos. 

Los requisitos necesarios son:

- [x] Java Runtime Environment SE 1.8.0
- [x] Microsoft Visual C++ 2017 Redistributable
- [x] Microsoft Visual C++ 2019 Refistributable
- [x] Windows Installer 3.1 Redistributable

## Información del cliente

Para la instalación es necesario introducir los datos de nombre de usuario, organización y número de serie. Esta información es irrelevante para el software, pero es una información importante si quieres definir y distinguir diferentes instalaciones de PowerStudio.

![](/es/_custom/img/install_03.jpg)

## Tipo de instalación

El instalador permite realizar uan instalación completa o hacer una instalación personalizada para instalar sólo ciertas partes del software. Recomendamos siempre hacer la instalación **COMPLETA**.

![](/es/_custom/img/install_04.jpg)

## Puertos personalizados

Por defecto se instalarán los servicios necesarios en ciertos puertos. Algunos de ellos no se pueden cambiar, pero otros se peuden personalizar en caso de que los propuestos estén en uso. Los puertos personalizables son los puertos de los servicios de **Wave**, **Identity** y **Administrator**. 

Para saber la funcionalidad de cada servicio, puedes consultar la sección "Microservicios" de esta documentación.

![](/es/_custom/img/install_05.jpg)

## Iniciar la instalación

Después de proporcionar la información necesaria, la instalación está lista para iniciarse.

Hacer click en el botón "**Iniciar**" para iniciar la instalación.

![](/es/_custom/img/install_06.jpg)

## Instalando

La instalación se inicia y se instala todo el paquete PowerStudio.

![](/es/_custom/img/install_07.jpg)

## Finalizar

Cuando la instalación finalice, la pantalla de instalación completa se mostrará. Al hacer click en "Finalizar", se cerrará el instalador y PowerStudio estará instalado en el sistema.

![](/es/_custom/img/install_08.jpg)

Es posible que tras la instalación, el sistema necesite reiniciar. En este caso, se mostrará un mensaje de reinicio. Recomendamos reiniciar siempre antes de ejecutar PowerStudio por primera vez.