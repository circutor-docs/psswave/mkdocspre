# Acceso

La instalación de PowerStudio incluye la instalación de una serie de aplicaciones y toda la arquitectura necesaria para el funcionamiento del software. PowerStudio Wave es una de las aplicaciones instaladas.

## Acceso a PowerStudio Wave

Se puede acceder a PowerStudio Wave de dos maneras:

=== "Desde menú inicio"

    ![](/es/_custom/img/access_startmenu.jpg){ align=left width=250 }

    A través del menú de inicio de Windows, se puede acceder a **PowerStudio Wave** ejecutando el acceso directo que se ha creado en la carpeta Circutor.

    !!! tip "Consejo"

        Puedes crear un **acceso directo** para abrir directamente PowerStudio Wave en tu navegador predeterminado.

=== "Desde el navegador"

    También se puede acceder a PowerStudio Wave directamente a través del navegador que prefieras. Para esto, accede a la URL de localhost desde tu navegador, accediendo por el puerto configurado en la instalación.

    Introduce en la barra de direcciones del navegador la siguiente ruta:

        https://localhost:8091

    Por defecto, el puerto de PowerStudio Wave es el ==**8091**==. Si has cambiado el puerto durante la instalación, deberás acceder a la URL por el puerto que has especificado durante la instalación.

    ??? quote "¿Cómo sé si he instalado PowerStudio Wave en el puerto por defecto o lo he cambiado durante la instalación?"

        Para saber el puerto en el que se ha instalado el servicio de PowerStudio Wave, puedes acceder a PowerStudio Wave desde el acceso directo del menú inicio. De esta manera, en dirección del navegador podrás ver el puerto donde se ha instalado el servicio.

        ``` 
        https://localhost:8892 
        ```

        En este ejemplo, el puerto del servicio de PowerStudio Wave es el **8892**.

---

## La conexión no es privada

Cada vez que utilizas tu navegador web para acceder a un sitio, éste verifica la presencia de un certificado SSL. Los navegadores actuales necesitan estos certificados para asegurar la integridad y seguridad de los datos intercambiados con los sitios web.

En el caso de PowerStudio Wave, se utiliza un certificado autofirmado. Aunque este software se ejecuta localmente y no requiere un certificado SSL convencional, es importante tener en cuenta que algunos navegadores pueden mostrar una advertencia sobre la privacidad de la conexión debido a la ausencia de un certificado SSL estándar.

A continuación puedes ver la advertencia y los pasos en Chrome, Edge y Firefox.

=== "Google Chrome"

    ![](/es/_custom/img/access_chrome_ssl.jpg)

    Para continuar, es necesario completar el acceso a localhost.

    * Hacer click en el botón "**Configuración avanzada**"
    * Hacer click en el enlace "**Acceder a localhost (no seguro)**"

=== "Microsoft Edge"

    ![](/es/_custom/img/access_edge_ssl.jpg)

    Para continuar, es necesario completar el acceso a localhost.

    * Hacer click en el botón "**Avanzado**"
    * Hacer click en el enlace "**Continuar a localhost (no seguro)**"

=== "Mozilla Firefox"

    ![](/es/_custom/img/access_firefox_ssl.jpg)

    Para continuar, es necesario completar el acceso a localhost.

    * Hacer click en el botón "**Avanzado...**"
    * Hacer click en el enlace "**Aceptar el riesgo y continuar**"

!!! note "Nota"
    
    Esta configuración se recordará en el navegador y no se volverá a preguntar.

---

## Iniciar sesión

![](/es/_custom/img/login.jpg)

PowerStudio Wave gestiona dos tipos de usuario, un usuario administrador (**admin**), con todos los privilegios activados y un usuario invitado (**user**), con privilegios limitados.

Para la primera conexión, es necesario **conectar con el usuario administrador y activar el producto**.

Las credenciales por defecto de ambos usuarios son:

=== "Administrador"

    ```
    Usuario: admin
    Contraseña: admin1234
    ```

=== "Invitado"

    ```
    Usuario: user
    Contraseña: user1234
    ```

!!! tip "Consejo"

    Es recomendable cambiar las contraseñas de los usuarios "admin" y "user". Consulta la sección "[Usuarios](/settings/users)" para más información

---

## Activación

!!! example "Modo demostración"

    Mientras el producto no esté activado, sólo dispondrás de una versión de demostración de **60 minutos**.

### Adquirir una licencia

Si no dispones de una licencia de producto, deberás contactar con Circutor para adquirir una licencia. Consulta la sección "[Planes](/es/home/license)" para conocer las opciones para adquirir una licencia.

### Activar el producto

Si ya dispones de una licencia de producto, debes activar PowerStudio desde la gestión de licencias. Consulta el módulo de "[Licencias](/es/modules/license/online)" para saber las opciones de activación de PowerStudio.
