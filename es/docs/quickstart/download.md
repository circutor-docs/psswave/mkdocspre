# Descargar PowerStudio

**PowerStudio Wave** es una aplicación dentro del paquete **PowerStudio** que permite gestionar la integración de equipos CIRCUTOR desde el navegador de una forma fàcil y amigable. Para empezar a utilizar **PowerStudio Wave** es necesario descargar e instalar el paquete de aplicaciones PowerStudio.

## Descarga

Puedes descargar la última versión de PowerStudio desde aquí.

[Descarga la última versión 24.7.1 :octicons-arrow-right-24:](https://docs.circutor.com/docs/PowerStudio_Setup_24.7.1.0_241114_816_x64.zip){ .md-button .md-button--primary}

## Requisitos mínimos

* Procesador: **i5 de 10ª generación** (equivalente o superior)
* Memória RAM: **16 Gb**
* Almacenamiento: **2 Tb**
* Sistema operativo: **Windows Server** (2016 o superior) / **Windows Pro** (10 o superior)
* **Adaptador Ethernet**

## ¿Ya tienes una versión anterior instalada?

Si tienes una versión anterior instalada de PowerStudio es necesario **desinstalar** la versión anterior **ANTES** de instalar la nueva versión. 

!!! quote "Nota"

    Todas las configuraciones de la versión anterior no se perderán y estarán disponibles en la nueva instalación.

Una vez desinstalada cualquier versión anterior de PowerStudio, ya se puede instalar la nueva versión.

## Iniciar la instalación

Una vez descargado el software, deberás descomprimir el archivo ZIP e iniciar el asistente de instalación ejecutando el archivo comprimido.
    
```title="Nombre del archivo"
PowerStudio_Setup_24.x.x.x_x64.exe
```

!!! quote "Nota"

    La versión y la fecha están incluida en el nombre del archivo. Recomendamos comprobar que se va a instalar la última versión de PowerStudio. Puedes consultar la última versión en el [registro de cambios](/es/changelog/).