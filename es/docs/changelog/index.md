---
hide:
  - feedback
---
# Registro de cambios

## 24.7.1 <small><small>- 09/11/2024</small></small> 

* Mejoras en la gestión de la memoria del servicio PowerStudio.
* Mejoras en la gestión de la memoria de los microservicios.

## 24.7.0 <small><small>- 09/11/2024</small></small> 

* Las zonas ya aceptan controladores genéricos.
* Mejora de rendimiento del módulo de Monitor.
* Los controladores genéricos ahora soportan canales.
* Actualización del formato de fecha según ISO8601.
* Versión embebida para Line-EDS compatible con B100 y B150.

## 24.6.2 <small><small>- 15/10/2024</small></small> 

* Mejoras de rendimiento
* Corrección de la gestión de zonas horarias en embebidos.
* Corrección de errores en la exportación de aplicaciones sin pantallas ni informes.

## 24.6.1 <small><small>- 11/10/2024</small></small> 

* Corrección de errores en Windows cuando el motor pierde la comunicación.
* Correciones en variables calculadas no guardadas.

## 24.6.0 <small><small>- 07/10/2024</small></small> 

* Mejora del rendimiento de la CPU de los controladores virtuales.
* Mejora en el tiempo de exportación en aplicaciones grandes.
* Nuevo indicador de suceso habilitado/deshabilitado en la lista de sucesos del Editor.
* Nueva funcionalidad de importación de sucesos del Editor a Wave.
* Nueva funcionalidad para exportar e importar sucesos de Wave.
* Visualización de sucesos en Wave en formato tabla.
* Mejoras de interfaz de Wave.
* Corrección de errores en los límites de las licéncias.
* Corrección de errores en una instalación del Editor personalizada.
* Correcciones menores.

## 24.5.6 <small><small>- 10/09/2024</small></small> 

* Corrección en la visualización del árbol de dispositivos con controladores genéricos.

## 24.5.5 <small><small>- 06/09/2024</small></small> 

* Corrección de errores con los controladores genéricos sin imágenes.

## 24.5.4 <small><small>- 31/07/2024</small></small> 

* Correcciones en el motor con autenticación y comunicación https.

## 24.5.3 <small><small>- 25/07/2024</small></small> 

* Mejoras en el tiempo de descarga de las aplicaciones con motores inferiores en el editor.
* Corrección de problema a la hora de importar o exportar una aplicación en el editor, no funcionaba correctamente al utilizar usuario/contraseña.
* Versión embebida para Line-EDS compatible con módulo Line-M-4G.

## 24.5.2 <small><small>- 15/06/2024</small></small> 

* Correciones menores.

## 24.5.1 <small><small>- 08/06/2024</small></small> 

* Corrección de errores detectados en QNA412.

## 24.5.0 <small><small>- 05/06/2024</small></small> 

* Mejoras en la gestión de plantillas de primer nivel.
* Corrección de errores en el servicio de Administrator.
* Variables de Begin y End para controladores genéricos.

## 24.4.2 <small><small>- 10/05/2024</small></small>

* Mejoras de seguridad en entornos Windows server.
* Se permite la desactivacion permanente de los microservicios.

## 24.3.0 <small><small>- 18/04/2024</small></small>

* Se añade el CVM C11 Ethernet.
* Las variables calculadas se publican siempre en el bus de eventos.

## 24.2.1 <small><small>- 30/03/2024</small></small>

* Se mejora la identificacion del estado de los servicios.
* Correcciones menores.

## 24.2.0 <small><small>- 11/03/2024</small></small>

* Se añaden las variables de los embebidos en la herramienta WAVE.

## 24.1.5 <small><small>- 22/02/2024</small></small>

* Mejoras en la gestión de la memoria de los microservicios.

## 24.1.4 <small><small>- 13/02/2024</small></small>

* Corrección de error en el servicio PSS Admin.

## 24.1.3 <small><small>- 12/02/2024</small></small>

* Mejoras en la gestión de la base de datos SQL.

## 24.1.2 <small><small>- 09/02/2024</small></small>

* Mejoras en la gestión de las comunicaciones https.
* Correcciones menores.

## 24.1.1 <small><small>- 08/02/2024</small></small>

* Mejoras en herramienta Monitor.

## 24.1.0 <small><small>- 06/02/2024</small></small>

* Mejoras en la optimización de las tablas para base de datos SQL.

## 24.0.1 <small><small>- 03/02/2024</small></small>

* Mejoras en la comunicación del bus de eventos.

## 24.0.0 <small><small>- 01/02/2024</small></small>

* :party_popper: :party_popper: ¡Lanzamiento de PowerStudio Wave!