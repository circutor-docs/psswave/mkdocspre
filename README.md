# Install mkdocs pip extension (https://www.mkdocs.org/getting-started/)
pip install mkdocs

# Required pip modules (https://github.com/squidfunk/mkdocs-material)
pip install mkdocs-material