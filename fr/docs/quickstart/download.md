# Télécharger PowerStudio

**PowerStudio Wave** est une application intégrée au paquet **PowerStudio** et qui permet de gérer l’intégration d’équipements CIRCUTOR depuis le navigateur d’une façon simple et conviviale. Pour commencer à utiliser **PowerStudio Wave**, vous devez télécharger et installer le paquet d’applications PowerStudio.

## Téléchargement

Vous pouvez télécharger la dernière version de PowerStudio ici.

[Téléchargez la dernière version 24.1.4 :octicons-arrow-right-24:](https://docs.circutor.com/docs/PowerStudio_Setup_24.1.4.0_2402131223_x64.zip){ .md-button .md-button--primary}

## Configuration requise

* Processeur : **i5 de 10e génération** (équivalent ou ultérieur)
* Mémoire RAM : **16 Go**
* Stockage : **2 To**
* Système d'exploitation : **Windows Server** (2016 ou ultérieur) / **Windows Pro** (10 ou ultérieur)
* **Adaptateur Ethernet**

## Vous avez déjà une version antérieure installée ?

Si vous avez une version antérieure installée de PowerStudio, vous devez **désinstaller** la version antérieure **AVANT** d’installer la nouvelle.

!!! quote "Remarque"

    Toutes les configurations de la version antérieure seront conservées et disponibles dans la nouvelle installation.

Après avoir désinstallé une version antérieure de PowerStudio, vous pouvez installer la nouvelle.

## Démarrer l’installation

Une fois le logiciel téléchargé, vous devez décompresser le fichier ZIP et lancer l’assistant d’installation en exécutant le fichier compressé.

```title="Nombre del archivo"
PowerStudio_Setup_24.x.x.x_x64.exe
```

!!! quote "Remarque"

    Le nom du fichier inclut la version et la date. Il est conseillé de vérifier que vous allez bien installer la dernière version de PowerStudio. Vous pouvez consulter la dernière version dans le [journal des modifications](/es/changelog/).