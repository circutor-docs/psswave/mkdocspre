# Guide d’installation

## Installation

Même si l’assistant d’installation vous guide dans le processus d’installation, ce guide vise à résoudre certaines étapes simples en apportant plus de détails.

## Message de sécurité

Certains antivirus peuvent détecter le fichier d’installation comme une application inconnue. Dans ce cas s’affiche un message d’avertissement. Vous devez quoi qu’il en soit exécuter l’application pour démarrer la procédure d’installation.

![](/es/_custom/img/install_warning.jpg)

## Bienvenue

Avant de démarrer l’installation, l’assistant demande des informations nécessaires pour l’installation. Une fois ces informations fournies, l’installation commence.

![](/es/_custom/img/install_01.jpg)

Pour avancer lors du processus d’installation, vous devez cliquer sur le bouton "**Siguiente**" (Suivant).

## Contrat de licence

Vous devez accepter le contrat de licence du logiciel pour poursuivre l’installation.

![](/es/_custom/img/install_02.jpg)

## Conditions prérequises

Pour fonctionner, PowerStudio a besoin de certaines conditions prérequises dans le système. Le programme d’installation vérifie et installe ces éléments requis avant d’installer PowerStudio. Il est inutile d’installer les éléments requis déjà disponibles.

Les éléments requis sont les suivants :

- [X] Java Runtime Environment SE 1.8.0
- [X] Microsoft Visual C++ 2017 Redistributable
- [X] Microsoft Visual C++ 2019 Redistributable
- [X] Windows Installer 3.1 Redistributable

## Informations sur le client

Pour l’installation, vous devez saisir le nom d’utilisateur, l’organisation et le numéro de série. Ces informations ne sont pas pertinentes pour le logiciel, mais elles sont importantes pour définir et distinguer les différentes installations de PowerStudio.

![](/es/_custom/img/install_03.jpg)

## Type d'installation

Le programme d’installation permet d’effectuer soit une installation complète, soit une installation personnalisée pour n’installer que certains composants du logiciel. Il est conseillé de toujours effectuer une installation **COMPLÈTE**.

![](/es/_custom/img/install_04.jpg)

## Ports personnalisés

Les services nécessaires sont par défaut installés dans certains ports. Certains ne sont pas modifiables, mais d’autres peuvent être personnalisés si ceux proposés sont en cours d’utilisation. Les ports personnalisables sont les ports des services de **Wave**, **Identity** et **Administrator**.

Pour connaître la fonctionnalité de chaque service, vous pouvez consulter la section "Microservices" de la présente documentation.

![](/es/_custom/img/install_05.jpg)

## Démarrer l’installation

Après avoir fourni les informations nécessaires, l’installation peut démarrer.

Cliquez sur le bouton "**Iniciar**" (Démarrer) pour démarrer l’installation.

![](/es/_custom/img/install_06.jpg)

## Instalando (En cours d'installation)

L’installation démarre et tous le paquet PowerStudio est installé.

![](/es/_custom/img/install_07.jpg)

## Finalizar (Terminer)

Au terme de l’installation s’affiche l’écran d’installation terminée. Cliquez sur "Finalizar" (Terminer) pour fermer le programme d’installation ; PowerStudio est alors installé sur le système.

![](/es/_custom/img/install_08.jpg)

Le système doit éventuellement être redémarré après l’installation. Dans ce cas, un message de redémarrage s’affiche. Il est conseillé de toujours redémarrer avant d’exécuter PowerStudio pou la première fois.