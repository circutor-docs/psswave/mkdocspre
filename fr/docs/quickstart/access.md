# Accès

L’installation de PowerStudio inclut celle d’une série d’applications et toute l’architecture nécessaire pour le fonctionnement du logiciel. PowerStudio Wave est l’une des applications installées.

## Accès à PowerStudio Wave

Vous pouvez accéder à PowerStudio Wave de deux façons :

=== "Depuis le menu Démarrer"

    ![](/es/_custom/img/access_startmenu.jpg){ align=left width=250 }
    
    Depuis le menu Démarrer de Windows, vous pouvez accéder à **PowerStudio Wave** en exécutant l’accès direct créé dans le dossier Circutor.
    
    !!! tip "Conseil"
    
        Vous pouvez créer un **accès direct** pour ouvrir directement PowerStudio Wave dans votre navigateur prédéterminé.

=== "Depuis le navigateur"

    Vous pouvez également accéder à PowerStudio Wave directement à travers le navigateur de votre choix. Pour ce faire, accédez à l’URL de localhost depuis votre navigateur via le port configuré dans l’installation.
    
    Saisissez dans la barre d’adresse du navigateur l’URL suivante :
    
        https://localhost:8091
    
    Par défaut, le port de PowerStudio Wave est ==**8091**==. Si vous avez changé le port au moment de l’installation, vous devrez accéder à l’URL via le port spécifié lors de l’installation.
    
    ??? quote "Comment savoir si j’ai installé PowerStudio Wave sur le port par défaut ou si je l’ai changé au cours de l’installation ?"
    
        Pour savoir sur quel port le service de PowerStudio Wave a été installé, vous pouvez accéder à PowerStudio Wave depuis l’accès direct du menu Démarrer. De cette façon, vous pourrez voir dans la barre d’adresse du navigateur le port où le service a été installé.
    
        ``` 
        https://localhost:8892 
        ```
    
        Dans cet exemple, le port du service de PowerStudio Wave est **8892**.

---


## La connexion n’est pas privée

Chaque fois que vous utilisez votre navigateur Web pour accéder à un site, il vérifie l’existence d’un certificat SSL. Les navigateurs actuels requièrent ces certificats pour garantir l’intégrité et la sécurité des données échangées avec les sites Web.

Dans le cas de PowerStudio Wave, un certificat autosigné est utilisé. Même si ce logiciel s’exécute en local et ne requiert pas un certificat SSL conventionnel, il est important de savoir que certains navigateurs peuvent afficher un avertissement sur la confidentialité de la connexion en raison de l’absence d’un certificat SSL standard.

Ci-après l’avertissement et la procédure dans Chrome, Edge et Firefox.

=== "Google Chrome"

    ![](/es/_custom/img/access_chrome_ssl.jpg)
    
    Pour continuer, vous devez terminer l’accès à localhost.
    
    * Cliquer sur le bouton "**Configuración avanzada**" (Configuration avancée)
    * Cliquer sur le lien "**Acceder a localhost (no seguro)**" (Accéder à localhost (non sécurisé))

=== "Microsoft Edge"

    ![](/es/_custom/img/access_edge_ssl.jpg)
    
    Pour continuer, vous devez terminer l’accès à localhost.
    
    * Cliquer sur le bouton "**Avanzado**" (Avancé)
    * Cliquer sur le lien "**Continuar a localhost (no seguro)**" (Continuer à localhost (non sécurisé))

=== "Mozilla Firefox"

    ![](/es/_custom/img/access_firefox_ssl.jpg)
    
    Pour continuer, vous devez terminer l’accès à localhost.
    
    * Cliquer sur le bouton "**Avanzado...**" (Avancé...)
    * Cliquer sur le lien "**Aceptar el riesgo y continuar**" (Accepter le risque et continuer)

!!! note "Remarque"

    Cette configuration sera enregistrée dans le navigateur et la question ne sera plus posée.

---


## Démarrer une session

![](/es/_custom/img/login.jpg)

PowerStudio Wave gère deux types d’utilisateur : un utilisateur administrateur (**admin**) avec toutes les autorisations activées, et un utilisateur invité (**user**) avec des autorisations limitées.

Pour la première connexion, vous devez vous **connecter en tant qu’utilisateur administrateur et activer le produit**.

Les données d’identification par défaut des deux utilisateurs sont :

=== "Administrateur"

    ```
    Utilisateur : admin
    Mot de passe : admin1234
    ```

=== "Invité"

    ```
    Utilisateur : user
    Mot de passe : user1234
    ```

!!! tip "Conseil"

    Il est conseillé de changer les mots de passe des utilisateurs "admin" et "user". Consultez la section "[Utilisateurs](/settings/users)" pour en savoir plus

---


## Activation

!!! example "Mode démonstration"

    Tant que le produit n’est pas activé, vous bénéficiez seulement d'une version de démonstration de **60 minutes**.

### Acquérir une licence

Si vous ne possédez pas une licence du produit, vous devez contacter Circutor pour en acquérir une. Consultez la section [Plans](/es/home/license) pour découvrir les options pour acquérir une licence.

### Activer le produit

Si vous possédez déjà une licence du produit vous devez activer PowerStudio depuis le gestionnaire de licences. Consultez le module "[Licences](/es/modules/license/online)" pour découvrir les options d’activation de PowerStudio.