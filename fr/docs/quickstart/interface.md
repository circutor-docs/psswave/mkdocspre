# Interface utilisateur

## Introduction

L’écran principal de PowerStudio Wave est divisé en **quatre** espaces : **En-tête**, **Navigation**, **Contenu** et **Bas de page**.

![](/es/_custom/img/interface_screen.jpg)

## En-tête

En haut à droite de l’en-tête se trouvent les options pour changer de langue et fermer la session. Vous pouvez aussi voir l’utilisateur ayant démarré la session.

![](/es/_custom/img/interface_header.jpg)

### Changement de langue

PowerStudio Wave est actuellement disponible dans 3 langues : espagnol, anglais et catalan. Pour changer de langue, il suffit d’ouvrir la liste déroulante et de choisir la langue souhaitée.

### Fermer la session

Dans l’angle supérieur droit, à côté du nom de l’utilisateur ayant démarré la session, se trouve l’icône pour **fermer la session**. Si vous cliquez sur cette icône, la session se ferme et l’écran de **démarrage de session** s’affiche automatiquement.

## Navigation

Sur la gauche de l’écran se trouve le panneau de navigation pour l’ensemble des modules et des fonctionnalités de PowerStudio Wave. Tous sont expliqués en détail dans les [modules](/es/modules/) de la présente documentation.

![](/es/_custom/img/interface_navigation.jpg)

À mesure que sont étendues les fonctionnalités de PowerStudio Wave, la navigation évolue et s’adapte pour offrir un accès pratique et structuré à toutes les fonctionnalités.

## Bas de page

Dans la partie inférieure de l’écran se trouve le bas de page. Vous pouvez y consulter la section "Acerca de" (À propos), activer l’aide générique et masquer/afficher le panneau de navigation et l’en-tête pour que le contenu occupe tout l’écran.

![](/es/_custom/img/interface_footer.jpg)

### Plein écran

L’icône de plein écran masque/affiche le panneau de navigation et l’en-tête pour libérer de l’espace pour le contenu et travailler ainsi de façon plus commode.

Si vous appuyez sur la touche F11, le navigateur passe en plein écran, ce qui vous offre davantage d’espace pour le contenu.