# PowerStudio Wave

Découvrez les principales nouveautés de la nouvelle interface Web pour PowerStudio.

## Une nouvelle expérience utilisateur

Le nouvel outil **Wave** (Web App de Visualisation et d’Édition) de PowerStudio est une interface utilisateur innovante qui présente les nouvelles fonctionnalités et vous permet d’interagir avec PowerStudio d’une façon plus simple, flexible et pratique.

Wave vous offre une expérience utilisateur améliorée, avec une navigation intuitive et une interface visuellement attractive qui simplifie la configuration et l’édition de votre application. Vous pouvez à présent tirer le meilleur parti de PowerStudio depuis n’importe quel navigateur Web.

Wave offre une variété de modules que vous permettront d’optimiser son utilité :

* Création de **pilotes** catégorisés.
* Conception de **tableaux de bord** personnalisés.
* Configuration d’**événements/alarmes** via une interface visuelle et intuitive.
* **Exportation de données** en temps réel de variables enregistrées vers des serveurs SQL.
* Intégration de l’outil **Engine Manager** dans la nouvelle interface Wave.
* Surveillance de **services** et de journaux système.

<hr />
    
Pour en savoir plus sur chaque fonctionnalité, consultez [Modules](/fr/modules/).
