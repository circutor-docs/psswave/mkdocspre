# Plans

Cette section contient toute les informations liées aux plans, aux compléments de PowerStudio, ainsi qu’à l’achat d’une licence logicielle Circutor.

## Versions

Pour gérer efficacement n’importe quel type de projet, PowerStudio offre **trois versions** qui s’adaptent à votre activité en fonction de vos besoins. Toutes disposent de l’ensemble des éléments composant PowerStudio, à savoir l’éditeur, le moteur et le client, ainsi que le nouvel outil WAVE, avec toutes les fonctionnalités détaillées dans la section [Modules](/es/modules/).

Choisissez la version la plus adéquate en fonction des équipements que vous devez gérer :

* PowerStudio Scada **Basic** jusqu’à 25 équipements.
* PowerStudio Scada **Pro** jusqu’à 50 équipements.
* PowerStudio Scada **Ultimate** jusqu’à 500 équipements.
* PowerStudio Scada **Enterprise** plus de 500 équipements.



Tous les plans sont compatibles avec des équipements Circutor ou de toute autre marque avec des communications Modbus.

![](/es/_custom/img/plans2.jpg)

Si vous souhaitez étendre ultérieurement votre licence, ce processus sera simple et flexible, sans aucune complication.

## Comment acquérir votre licence

Si vous souhaitez acquérir votre licence PowerStudio, vous pouvez vous adresser à votre canal d’achat habituel. Si vous contactez Circutor pour la première fois, veuillez envoyer votre demande à l’adresse **iot@circutor.com** ou remplir le formulaire suivant :

[Formulaire de demande :octicons-arrow-right-24:](https://circutor.com/contacto-iot/){:target="\_blank" .md-button .md-button--primary}

Nous serons ravis de vous répondre et de vous aider à trouver la solution la plus adaptée à vos besoins.

## Complément - Serveur OPC-UA

Vous pouvez transformer votre logiciel PowerStudio en un serveur de données OPC-UA grâce à ce connecteur de données. Si vous voulez travailler avec votre propre Scada OPC, choisissez quelles variables de PowerStudio vous souhaitez publier sur le serveur OPC et envoyez les demandes voulues en temps réel. Analysez l’efficacité énergétique de votre bâtiment où vous voulez, quand vous voulez.

![](/es/_custom/img/plans_opc.jpg)