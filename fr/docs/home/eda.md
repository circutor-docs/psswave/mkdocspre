# Architecture orientée événements

PowerStudio Wave se base sur une nouvelle architecture distribuée de **microservices** et **orientée événements**.

Cette nouvelle architecture élimine les restrictions d’extensibilité des versions antérieures de PowerStudio (4.x), ce qui permet une exécution plus rapide et plus efficace des fonctionnalités, notamment lorsque de nombreux utilisateurs s’en servent simultanément.

Cette nouvelle conception interne permet également d’ajouter de nouvelles fonctionnalités et de corriger des problèmes détectés en altérant le moins possible le fonctionnement général du produit, ce qui simplifie ainsi le travail de développement.

En résumé, la nouvelle architecture de **PowerStudio Wave** permet que le produit évolue et s’adapte mieux aux besoins des clients.

## Aspects techniques

Les différentes fonctionnalités sont implémentées dans des **microservices**, ou services spécialisés dans une tâche unique, qui s’exécutent de façon distribuée et qui échangent des informations sous forme d’événements.

Par ailleurs, chaque microservice répond uniquement aux événements spécifiques nécessaires pour implémenter sa fonctionnalité.

L’infrastructure nécessaire pour pouvoir exécuter PowerStudio Wave a été légèrement étendue. Ce point est à prendre en compte sachant qu’un système de messagerie asynchrone est requis pour permettre la communication entre les microservices : il s’agit d’un agent de messages, également appelé "bus d’événements". **RabbitMQ** a été retenu.

## Schéma de l’architecture

![](/es/_custom/img/architecture_eda.jpg)

## Principaux microservices

Toutes les fonctionnalités de PowerStudio 4.x restent disponibles dans **PowerStudio Wave**, certaines étant déjà implémentées dans des microservices dédiés, d’autres encore en attente de migration. Des fonctionnalités inédites ont en outre été ajoutées dans de nouveaux microservices.

### Fonctionnalités de PSS4.x

- [ ] Éditeur de PowerStudio
- [ ] Client JAVA de PowerStudio
- [ ] Client HTML5 de PowerStudio
- [ ] Moteur de PowerStudio

***


### Fonctionnalités dans les microservices

- [X] PowerStudio Wave
- [X] Exportateur de données
- [X] Programmeur d’exportation
- [X] Événements
- [X] Moniteur
- [X] Pilotes génériques
- [X] Licences
- [X] Analyse de données