# Filtres

Ici, il suffit de sélectionner le dispositif dans la liste "**Elementos Disponibles**" (Éléments disponibles) et de le passer à la liste de droite ("Elementos Seleccionados" (Éléments sélectionnés)). SI vous double-cliquez ensuite sur le dispositif sélectionné ou sur l’icône d’engrenage, une fenêtre s'ouvre pour y sélectionner les variables du dispositif en question.

![](/es/_custom/img/dataexport_filters.jpg)

Dans cet écran, passez à droite les variables que vous voulez exporter. Si à l’étape de configuration de la base de données, vous avez déjà sélectionné le démarrage de l’exportation, celle-ci débute immédiatement quand vous cliquez sur le bouton "Aplicar" (Appliquer).