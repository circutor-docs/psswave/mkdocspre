---
hide:
  - feedback
---
# Exportation de données

Le module d’exportation de donnée permet d’exporter les données de la base de données de PowerStudio vers une base de données SQL Server. La version de Microsoft SQL Server doit être 2008 ou ultérieure.

Par le biais de ce module, les données sont intégrées dans la base de données SQL au même moment que leur enregistrement dans PowerStudio.

Vous pouvez également programmer une exportation des données historiques de la base de données de PowerStudio vers SQL.

Pour le bon fonctionnement de ce module, il est nécessaire d’effectuer une [configuración](setup.md) de la base de données (propriété de l’utilisateur, non fournie avec le logiciel) et de définir des [filtres](filters.md) pour exporter les valeurs des variables et des dispositifs.