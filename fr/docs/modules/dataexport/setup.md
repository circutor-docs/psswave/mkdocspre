# Configuration

La section "**Base de Datos**" (Base de données) permet de configurer la base de données en indiquant le serveur, l’utilisateur et le mot de passe pour vous connecter à l’environnement de la base de données de l’équipement et créer la nouvelle base de données d’exportation.

!!! note "Remarque"

    Sachant qu’il s’agit de la base de données propriétaire de l’utilisateur, l’exportation n’est pas possible tant que la "chaîne de connexion" n’est pas indiquée.

![](/es/_custom/img/dataexport_setup.jpg)

Dans le champ "**Servidor**" (Serveur), ajoutez le **nom du serveur** de SQL Server qui est installé sur la machine de l’utilisateur, et saisissez l’**utilisateur** et le **mot de passe** dans les champs respectifs.

Le bouton "++Iniciar exportación" (Démarrer l’exportation) devient ainsi actif ; cliquez dessus pour recevoir les informations du moteur++.

![](/es/_custom/img/dataexport_start_button.jpg)

L’étape suivante consiste à définir les filtres indiquant quelles données de quelles variables vous souhaitez exporter.