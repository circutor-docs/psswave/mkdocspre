# SQLDataExport et PSSDataExport

## SQLDataExport - Structure

SQLDataExport est un programme qui s’installe séparément de PowerStudio y qui se programme pour qu’il importe les données historiques de certaines variables à partir d’une date ou pour une période de temps déterminée.

Sa structure de base de données est la suivante :

![](/es/_custom/img/sqldataexport_schema1.jpg)

il compte principalement des tables de configuration (moteurs, appareils, variables) dans lesquelles, outre les paramètres de l’entité (nom, description, URI, port, etc.), figurent les paramètres de programmation du téléchargement.

Il inclut par ailleurs les tables de valeurs comme "**std\_wat\_values**", avec les valeurs standard (STD et Watt).

Il existe d’autres tables de valeurs, comme celle d’événements du dispositif, d’événements de l’utilisateur et de fermetures de dispositifs.

## PSSDataExport - Structure

PSSDataExport est le service inclus dans PowerStudio Wave, qui une fois configuré pour tous les dispositifs et leurs variables ou pour une sélection définie par l’utilisateur, télécharge les données historiques en **temps réel** dans une nouvelle base de données SQL Server choisie par l’utilisateur.

![](/es/_custom/img/sqldataexport_schema2.jpg)

Les principales tables de données sont celles contenant les valeurs pour les moteurs, pour les dispositifs et pour chacune des variables ; leurs données standard figurent dans la table "**StandardValues**".

De façon similaire mais plus complète, vous trouverez des tables pour les données historiques d’événements de qualité, d’événements de dispositifs, d’événements d’utilisateur, ainsi que les harmoniques fondamentales. Si l’application inclut des dispositifs avec des fermetures, leurs valeurs se trouveront dans la table "**InvoiceClosureValues**".

L’autre différence avec le programme antérieur d’assistance SQLDataExport est que dans le microservice PSSDataExport, les valeurs historiques des **groupes de calculs** et leurs variables possèdent leurs propres tables différenciées des variables de dispositifs.

![](/es/_custom/img/sqldataexport_schema3.jpg)