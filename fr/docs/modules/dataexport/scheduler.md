# Programmeur de l’exportation de données

Le module de programmeur vous permet d’exporter des données historiques du moteur antérieures à l’installation de PowerStudio. Par exemple, si vous avez des valeurs historiques d’années antérieures à cette version, vous pouvez créer des programmations d’exportations en indiquant au moteur la période que vous voulez exporter vers votre nouvelle base de données.

![](/es/_custom/img/dataexport_scheduler_list.jpg)

## Configuration d’une programmation d’exportation

Si vous cliquez sur ++"Nuevo" (Nouveau), une fenêtre contextuelle apparaît avec un outil de configuration++.

Fixez tout d’abord l’intervalle des données avec les champs "**Fecha inicial**" (Date de début) et "**Fecha final**" (Date de fin). Pour ne pas saturer le moteur, indiquez les **heures** des **jours** auxquelles cette opération doit s’effectuer, sachant qu’en fonction des données à exporter, les tâches peuvent s’avérer très onéreuses.

![](/es/_custom/img/dataexport_scheduler_modal.jpg)

Pour finir, vous pouvez configurer l’adresse IP et le port du moteur si vous ne souhaitez pas ceux par défaut, ainsi que vos données d’identification, le cas échéant.