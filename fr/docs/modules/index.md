---
hide:
  - feedback
---
# Introduction aux modules

**PowerStudio Wave** dispose de modules que vous permettent de configurer les nouvelles fonctionnalités de PowerStudio et différents aspects de l’application.

Vous trouverez ici les informations nécessaires pour utiliser les modules. Découvrez toute la puissance de PowerStudio WAVE dans cette section.