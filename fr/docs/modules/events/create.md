# Comment créer un événement

Vous découvrirez dans cette section comment créer pas à pas un événement dans PowerStudio Wave. À chaque étape, vous pourrez par ailleurs accéder à toutes les informations détaillées sur chaque section.

## Étape 1 : Créer un événement

Dans l’écran principal d’événements, vous pouvez créer un nouvel événement en cliquant sur le bouton "Nuevo" (Nouveau) de la barre d’outils.

![](/es/_custom/img/events_create_new_button.jpg)

## Étape 2 : Données de base

Dans l’écran de création d’événements, vous devez tout d’abord lui attribuer un nom. Vous pouvez également créer et ajouter des catégories pour organiser vos événements et les filtrer plus facilement. Pour continuer, cliquez sur le bouton "Guardar" (Enregistrer).

![](/es/_custom/img/events_create_main_modal.jpg)

## Étape 3 : Mode édition

Quand vous créez un événement, il apparaît à l’état "**detenido**" (arrêté). Les diagrammes des événements à l’état "detenido" s’affichent dans une apparence différente. Dans cet état, vous pouvez éditer un diagramme de l’événement. De plus, les options en haut à droite de l’événement sont disponibles, comme "Duplicar" (Dupliquer) ou "Eliminar" (Supprimer).

![](/es/_custom/img/events_create_diagram_new_stopped.jpg)

## Étape 4 : Retard

Dans les premiers nœuds du diagramme, vous pouvez configurer le **retard**, tant **à l’activation** comme **à la désactivation** de l’événement. Pour ce faire, vous devez cliquer sur le nœud et configurer le retard en secondes.

![](/es/_custom/img/events_create_diagram_delay.jpg)

Vous pouvez consulter des informations détaillées sur le [nœud de retard](/es/modules/events/details/delay/).

## Étape 5 : Conditions

Dans les nœuds de condition, vous pouvez configurer les **conditions** devant s’appliquer pour que l’événement s’active. Pour ce faire, vous devez cliquer sur le nœud avec le symbole (+) et configurer les conditions. Les conditions sont exprimées dans l’éditeur d’expressions, et vous pouvez utiliser des variables et/ou des expressions logiques.

![](/es/_custom/img/events_create_diagram_conditional.jpg)

Vous pouvez consulter des informations détaillées sur le [nœud de condition](/es/modules/events/details/conditionals/).

## Étape 6 : Opérateur

Dans le nœud d’opérateur, vous pouvez choisir si l’opérateur est "AND" ou "OR". Pour ce faire, vous devez cliquer sur le nœud et sélectionner l’opérateur souhaité. L’opérateur sélectionné est appliqué et agit sur toutes les conditions précédentes.

![](/es/_custom/img/events_create_diagram_operator.jpg)

## Étape 7 : Actions

Depuis chaque ligne principale (à l’activation et à la désactivation) et après les conditions, vous pouvez configurer des actions **à l’activation** ou **pendant l’activation**. Vous pouvez créer ou modifier dans chacune de ces branches les actions qui s’exécuteront dans chaque cas.

![](/es/_custom/img/events_create_diagram_action.jpg)

Il existe trois types d’actions :

* Forcer une variable
* Envoyer un courrier électronique
* Envoyer un message Telegram

Chacune des actions possède son propre écran de configuration.

Vous pouvez consulter des informations détaillées sur les [actions](/es/modules/events/details/actions/).

## Étape 8 : Lancer un événement

Une fois l’événement configuré, vous devez le lancer pour que son évaluation commence et qu’il exécute les actions si besoin est. Pour lancer un événement, il suffit de cliquer sur l’icône de lecture des actions de l’événement. Lorsqu’un événement est à l’état "detenido" (arrêté), vous pouvez l’éditer, le dupliquer ou le supprimer.

![](/es/_custom/img/events_create_play_button.jpg)

Un événement lancé apparaît dans une couleur différente et ne peut pas être édité. Pour éditer un événement lancé, vous devez d’abord l’arrêter en cliquant sur l’icône d’arrêt.

![](/es/_custom/img/events_create_stop_button.jpg)