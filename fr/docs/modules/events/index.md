---
hide:
  - feedback
---
# Événements

Le module d’événements permet de configurer des actions dans PowerStudio. Grâce à ce module, vous pouvez programmer une section en fonction de l’état de n’importe quelle variable du système.

Créez des alarmes vous informant d’une valeur anormale via une notification par e-mail ou Telegram.

Programmez la connexion ou la déconnexion de n’importe quel circuit en fonction de l’heure, du jour ou de la valeur d’une variable.

Le module d’événements vous permet par conséquent de programmer les actions de contrôle de votre logiciel SCADA dans PowerStudio.