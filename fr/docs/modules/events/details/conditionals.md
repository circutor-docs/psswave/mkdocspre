# Conditions des événements

Lorsque vous créez ou modifiez la condition d’un événement, un formulaire apparaît pour indiquer l’identificateur, le moteur sur lequel s’exécute la condition, et l’expression.

!!! note "Remarque"

    Pour afficher la condition d’un événement dans un organigramme, vous pouvez choisir d’indiquer le texte de l’identificateur ou l’expression. Vous pouvez activer ou désactiver le bouton "Mostrar identificador" (Afficher l’identificateur) depuis le formulaire.

![](/es/_custom/img/events_conditional_modal.jpg)

Dans le champ "**Expresión**" (Expression), vous pouvez ajouter des variables d’équipements enregistrés dans PowerStudio et des opérateurs logiques, comme supérieur ou égal à… (>=), inférieur à… (\<) ou égal à… (=), entre autres. Les variables de dispositifs sont simples à identifier car elles sont définies entre crochets.

Vous pouvez également ajouter des fonctions de tout type pour créer des expressions et des conditions. Quand vous sélectionnez les fonctions que vous voulez utiliser, un encadré apparaît sur la droite avec des informations d’utilisation de ces fonctions et les paramètres nécessaires pour le bon fonctionnement.