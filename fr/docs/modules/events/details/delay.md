# Retards

Dans PowerStudio Wave, les événements permettent de définir un retard d’activation et de désactivation. Ces retards peuvent être des expressions qui sont vérifiées dès que l’événement se produit et à partir du moment où l’événement n’est plus valide.

![](/es/_custom/img/events_delay_modal.jpg)

## Retard d’activation

Cette valeur correspond au **retard d’activation** de l’événement en secondes. L’événement est alors activé quand se vérifie la condition d’activation pendant **au moins** le temps obtenu en calculant l’expression saisie dans ce champ au moment où l’événement devient valide.

## Retard de désactivation

Cette valeur correspond au **retard de désactivation** de l’événement en secondes. L’événement est alors désactivé quand, alors qu’il est activé, la condition d’activation n’est plus vérifiée **au moins** le temps indiqué selon l’expression dans ce champ (le calcul du temps s’effectue à partir du moment où l’événement n’est plus valide), de façon analogue au retard d’activation.