# Actions des événements

## Que sont les actions des événements ?

Une action est le résultat de l’exécution d’un événement dans Wave. Cette action a lieu quand se produit ou prend fin un événement selon ce que l’utilisateur a défini.

## Comment fonctionnent les actions des événements dans PowerStudio Wave ?

Pour qu’une action s’exécute, il faut d’abord générer une condition, expliquée plus loin.

Prenons un exemple pour mieux illustrer.

Si l’énergie active d’un dispositif est inférieure ou égale à 0, nous recevrons un message de notification pour nous avertir que ce dispositif n’est pas en train de fonctionner.

![](/es/_custom/img/events_action_diagram_example.jpg)

Dans ce cas, nous avons un événement qui s’activera si l’énergie active du dispositif CVM-C10 est à 0 et qui enverra un e-mail d’avertissement à l’adresse iot@circutor.com

![](/es/_custom/img/events_action_detail_activation.jpg)

Si l’énergie de l’appareil est rétablie et que la valeur n’est plus de 0, nous avons établi qu’un e-mail d’avertissement nous soit envoyé pour savoir que l’appareil reçoit de nouveau de l’énergie.

![](/es/_custom/img/events_action_detail_deactivation.jpg)

## Critères d’activation

Il existe 4 critères d’activation au moment de créer un événement :

* **À l’activation**: cette action s’exécute dès que l’événement défini est True et au terme du temps de retard d’activation défini auparavant.

* **Pendant l’activation**: cette action s’exécute pendant que l’événement défini est True et au terme du temps de retard d’activation défini auparavant.

* **À la désactivation**: cette action s’exécute dès que l’événement défini cesse de se produire (False) et au terme du temps de retard d’activation défini auparavant.

* **Pendant la désactivation**: cette action s’exécute pendant que l’événement défini est False et au terme du temps de retard d’activation défini auparavant.

## Types d’actions

Ci-après les différentes actions des événements dans PowerStudio Wave :

![](/es/_custom/img/events_action_options_modal.jpg)

### Forçage de variable

Comme l’indique son nom, le forçage de variable sert à forcer l’exécution d’une variable d’un dispositif. Dans ce cas par exemple, si une fonction est exécutée, le relais d’un EDS est ouvert (il faut alors savoir que l’état ouvert d’un relais est 0 et l’état fermé 1).

![](/es/_custom/img/events_action_force_modal.jpg)

* **Identificateur**: cet outil permet de montrer de façon simple et évolutive la variable pouvant être forcée. Pour le voir à l’écran, l’option "Mostrar identificador" (Afficher l’identificateur) doit être sélectionnée :

* **Motor (Moteur)**: il s’agit du moteur où se trouve dans ce cas la variable pouvant être forcée.

* **Variable**: il s’agit de la variable définie et qui s’exécute quand se produit ou cesse de se produire l’événement en fonction de ce que vous avez choisi auparavant. Un bouton d’assistant permet d’éviter d’écrire manuellement la variable.

* **Valor (Valeur)**: vous saisissez dans ce champ le résultat de la variable pouvant être forcée. La valeur est dans ce cas 0 car elle sert à ouvrir ce relais.

### Envoi d’e-mails

Grâce à cette option, vous pouvez envoyer des e-mails d’avertissement depuis PowerStudio Wave aux adresses électroniques indiquées : par exemple si la communication est perdue avec un appareil ou si un dispositif donne des valeurs incorrectes selon votre critère.

![](/es/_custom/img/events_action_email_modal.jpg)

!!! warning "Important"

    Avant toute chose, il est important de configurer un serveur de messagerie. Vous pouvez consulter ces informations dans la section de [configuration de la messagerie](/es/modules/settings/comms).

* **Identificador (Identificateur)**: cet outil permet de montrer de façon simple et évolutive la variable pouvant être forcée. Pour le voir à l’écran, l’option "Mostrar identificador" (Afficher l’identificateur) doit être sélectionnée :

* **De**: adresse électronique à laquelle vous envoyez cet e-mail et que vous avez définie auparavant dans la section Communications.

* **Para (À)**: la ou les adresses électroniques auxquelles vous allez envoyer l’e-mail.

!!! info "Informations"

    Séparer les adresses par "," sans espaces

* **Asunto (Objet)**: objet de l’e-mail.

* **Mensaje (Message)**: contenu de l’e-mail que vous recevez.

### Envoi de Telegram

Cet outil permet d’envoyer des avertissements au chat de Telegram de votre choix. Vous devez dans ce cas créer un bot de Telegram et obtenir le token de ce bot. Pour en savoir plus sur la configuration de Telegram, vous pouvez consulter la section de [configuration de Telegram](/es/modules/settings/comms).

![](/es/_custom/img/events_action_telegram_modal.jpg)

* **Identificador (Identificateur)**: cet outil permet de montrer de façon simple et évolutive la variable pouvant être forcée. Pour le voir à l’écran, l’option "Mostrar identificador" (Afficher l’identificateur) doit être sélectionnée :

* **Identificador de Bot de Telegram** (**Identificateur de bot de Telegram)** : vous devez ici choisir le bot de Telegram créé auparavant.

* **Mensaje (Message)**: message de notification qui arrive au chat de Telegram.