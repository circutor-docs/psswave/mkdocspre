# Événements

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctoe massa, nec semper lorem quam in massa.

## Événements

Quand vous accédez aux événements via le menu de navigation, la liste des événements disponibles s’affiche. Pour créer un nouvel événement, vous devez cliquez sur ++"Nuevo" (Nouveau)++.

![](/es/_custom/img/events_create_new_button.jpg)

Remplissez ensuite le formulaire avec l’identificateur ou le nom de l’événement. Vous pouvez aussi ajouter une ou plusieurs étiquettes pour filtrer une recherche par la suite. Pour finir, pour enregistrer l’événement, cliquez sur ++"Guardar" (Enregistrer)++.

Par défaut apparaît un organigramme avec toutes les options possibles pour créer un nouvel événement. Ce nouvel événement est par défaut à l’état **detenido** (arrêté) et le reste jusqu’à ce qu’il soit activé à l’aide du bouton de lecture qui apparaît en haut à droite de la vue du diagramme d’événements. Vous pouvez à tout moment lancer/arrêter un événement.

![](/es/_custom/img/events_create_diagram_new_stopped.jpg)