# Activation en ligne

Pour activer PowerStudio, vous avez besoin d’une licence utilisateur. Consultez la section [Plans](/es/home/license/) pour acquérir une licence.

Votre licence est fournie avec :

* **ID de licence**
* **Mot de passe de licence**

!!! warning "Important"

    Pour rappel, si vous effectuez une activation en ligne, l’ordinateur/le serveur où est installé PowerStudio SCADA doit toujours disposer d’une connexion à Internet.

## Activation

Activer une licence de PowerStudio en ligne est la façon la plus rapide et pratique d’activer votre licence.

Les licences en ligne requièrent une connexion à Internet pour être validées. Si le serveur dispose d’un accès limité à Internet, vous devez autoriser l’accès à l’URL **https://secure.softwarekey.com** via les ports **50786** et **443**.

Pour ce faire, procédez comme suit :

### 1\. Activer en ligne

Cliquez sur ++"Activación en línea" (Activation en ligne).++

![](/es/_custom/img/licenses_online_activation_button.jpg)

### 2\. Données de licence

Saisissez votre **ID de licence** et votre **mot de passe de licence**. Vous pouvez par ailleurs attribuer un **nom** à votre licence pour l’identifier plus facilement.

![](/es/_custom/img/licenses_online_modal.jpg)

### 3\. Activer une licence

Cliquez sur ++"Aceptar" (Accepter) pour activer votre++ licence.

## Désactivation

Pour désactiver une licence, vous cliquez sur ++"Desactivar" (Désactiver)++.

![](/es/_custom/img/licenses_deactivation_button.jpg)

## Actualiser une licence

Les licences activées en ligne peuvent être actualisées en cliquant sur "Refrescar" (Actualiser) afin d’actualiser leur état.

![](/es/_custom/img/licenses_refresh_button.jpg)