# Activation sans connexion

Pour activer PowerStudio, vous avez besoin d’une licence utilisateur. Consultez la section [Plans](/es/home/license/) pour acquérir une licence.

Votre licence est fournie avec :

* **ID de licence**
* **Mot de passe de licence**

## Activation

Activer une licence de PowerStudio sans connexion permet d’activer PowerStudio sur un ordinateur non connecté à Internet. Vous devez cependant disposer d’un autre terminal connecté à Internet, car vous devrez récupérer un code d’activation du fournisseur de licence pour la valider.

Pour ce faire, procédez comme suit :

### 1\. Activer sans connexion

Cliquez sur ++"Activación sin conexión" (Activation sans connexion).++

![](/es/_custom/img/licenses_offline_activation_button.jpg)

### 2\. Données de licence

Saisissez votre **ID de licence** et votre **mot de passe de licence**. Vous pouvez par ailleurs attribuer un **nom** à votre licence pour l’identifier plus facilement.

![](/es/_custom/img/licenses_offline_modal.jpg)

### 3\. Chaîne de demande

Cliquez sur le bouton ++"Generar cadena de petición" (Générer une chaîne de demande)++. Vous devez alors copier la chaîne de texte générée.

![](/es/_custom/img/licenses_offline_modal_generate_button.jpg)

### 4\. Terminal avec connexion

Depuis un autre terminal connecté à Internet, vous devez accéder à la page d’activation de licences du fournisseur. Dans un navigateur, saisissez l’adresse suivante :

`https://secure.softwarekey.com/solo/customers/ManualRequest.aspx`

### 5\. Valider la licence

Dans la page d’activation de licences, saisissez la chaîne de demande générée à l’étape 3 et cliquez sur ++"Submit" (Soumettre)++.

![](/es/_custom/img/licenses_softwarekey_1.jpg)

### 6\. Chaîne d’activation

La page affiche un code d’activation. Copiez ce code d’activation et collez-le dans le champ "**Cadena de activación**" (Chaîne d’activation) de l’écran d’activation de PowerStudio Wave.

![](/es/_custom/img/licenses_softwarekey_2.jpg)

### 7\. Activer une licence

Cliquez sur ++"Aceptar" (Accepter) pour activer votre++ licence.

## Désactivation

Pour désactiver une licence, vous cliquez sur ++"Desactivar" (Désactiver)++.

![](/es/_custom/img/licenses_deactivation_button.jpg)