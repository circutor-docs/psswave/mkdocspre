# Utilisation des données

PowerStudio Wave collecte les données d’utilisation pour améliorer l’expérience utilisateur. Les données recueillies concernent exclusivement l’utilisation du logiciel : il s’agit d’informations sur la façon dont il est utilisé et les éventuelles erreurs pouvant se produire. Ces données n’incluent pas d’informations personnelles ou de données sensibles.

## Politique d’utilisation

Ces données ne seront pas partagées avec des tiers et serviront uniquement à des fins d’analyse de Circutor. En acceptant cette politique, l’utilisateur accepte que le logiciel collecte et utilise ces données d’utilisation pour améliorer l’expérience utilisateur.

Si vous ne souhaitez pas participer à la collecte et à l’utilisation des données, vous pouvez choisir de désactiver cette option.

## Désactiver l’utilisation des données

Vous pouvez désactiver l’option de partage des données dans la fenêtre latérale accessible depuis l’écran de licences. Pour désactiver l’option, désélectionnez "J’accepte la collecte et l’utilisation des données".

![](/es/_custom/img/dataanalytics_main.jpg)