# Services

L’écran de services contrôle les services requis de PowerStudio Wave, tant les services IIS (Internet Information Services) que les services de moteur (services de Windows).

## Services IIS

Les services IIS présentent la liste de tous les services IIS dont PowerStudio a besoin. Chacun de ces services indique son état.

- **Iniciado (Démarré)**: le service est démarré et fonctionne sans problème.
- **Degradado (Dégradé)**: le service est démarré mais une de ses fonctions est dégradée.
- **Detenido (Arrêté)**: le service est arrêté.
- **Deteriorado (Détérioré)**: le service ne peut pas être démarré en raison d’un problème et doit être révisé.

Chaque service dispose d’actions pour son démarrage ou son arrêt depuis PowerStudio Wave sans devoir aller au gestionnaire de services de IIS. L’icône "info" affiche d’autres informations sur chaque service, comme l’état du disque, la mémoire et les bases de données associées.

![](/es/_custom/img/services_microservices.jpg)

## Moteurs

Les services de moteur présentent la liste des moteurs installés comme services de Windows. Comme pour les services IIS, chaque moteur indique son état :

- **Iniciado (Démarré)**: le service est démarré et fonctionne sans problème.
- **Degradado (Dégradé)**: le service est démarré mais une de ses fonctions est dégradée.
- **Detenido (Arrêté)**: le service est arrêté.
- **Deteriorado (Détérioré)**: le service ne peut pas être démarré en raison d’un problème et doit être révisé.

Comme auparavant, vous pouvez démarrer ou arrêter depuis PowerStudio n’importe quel service de moteur.

![](/es/_custom/img/services_engines.jpg)