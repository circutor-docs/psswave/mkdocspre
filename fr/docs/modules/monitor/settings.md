# Configuration du moniteur

**PowerStudio Wave** inclut l’outil **Moniteur** pour la création de tableaux de bord. Cette nouvelle fonctionnalité améliore l’expérience utilisateur en fournissant une visualisation intuitive et en temps réel des données et de l’état de votre installation.

Pour définir les paramètres de cet outil, accédez au sous-menu de configuration du moniteur.

## General (Général)

Cet onglet permet de définir les caractéristiques principales de la visualisation du moniteur :

* Afficher les conceptions dans le moniteur
* Activer la **rotation** des conceptions
* Configurer le temps de rotation
* Configurer le **temps d’actualisation** des données
* Configurer des titres, des couleurs et des images d’arrière-plan
* Bloquer la configuration de conceptions dans le visualiseur

![](/es/_custom/img/monitor_main.jpg)

## Imágenes (Images)

Cet onglet inclut le gestionnaire d’images du moniteur de PowerStudio Wave.

Ce gestionnaire permet de **stocker** les images que vous voulez utiliser, tant dans la configuration du moniteur que dans les propres widgets, et de **les organiser dans des dossiers**. Vous pouvez également **rechercher** à l’intérieur de chacun des dossiers.

![](/es/_custom/img/monitor_images.jpg)

## Datos en tiempo real (Données en temps réel)

Cet onglet présente les données en temps réel de toutes les variables appartenant à un widget.

![](/es/_custom/img/monitor_realtime.jpg)

## Administración (Administration)

Cet onglet permet d’importer et d’exporter des configurations du moniteur. Plus précisément, l’exportation génère un fichier **JSON** qui permet de répliquer la configuration entière du moniteur par le biais d’une importation dans un autre moniteur de PowerStudio WAVE.

![](/es/_custom/img/monitor_admin.jpg)

## Widgets

PowerStudio Scada WAVE permet d’afficher les informations souhaitées et dans le format de votre choix.

![](/es/_custom/img/monitor_widgets_screen.jpg)

### Diseños (Conceptions)

Le bouton ++"Añadir" (Ajouter) crée une nouvelle++ disposition.

À côté, le bouton ++"Clonar" (Cloner) est utile pour répliquer la disposition active et la modifier++ selon vos besoins.

Vous pouvez créer diverses dispositions ou écrans dans lesquels vous intégrez des widgets de la forme et de la taille de votre choix.

![](/es/_custom/img/monitor_layout_modal.jpg)

### Widgets disponibles

![](/es/_custom/img/monitor_widgets_list.jpg)

Les différents widgets disponibles sont les suivants :

* Widget de graphique
* Puissance actuelle
* Valeur simple
* Progression circulaire
* Widget d’image
* Caroussel d’images
* Widget html
* Widget rss
* Widget de jauge
* Groupe de widgets
* Widget personnalisable

!!! example "En développement"

    Cet outil est en phase de développement et les conceptions créées peuvent ne pas être compatibles avec les versions futures de PowerStudio WAVE.

### Insérer des widgets

Pour insérer chaque widget, il suffit de le faire glisser du menu latéral vers la disposition.

![](/es/_custom/img/monitor_widgets_drag.jpg)

### Configuration de widgets

Une fois les widgets insérés, vous pouvez choisir leur taille à l’aide des flèches de dimension.

![](/es/_custom/img/monitor_widgets_resize.jpg)

Cliquez sur l’icône d’**engrenage** pour ouvrir le menu de personnalisation de widget.

Dans ce menu, vous pouvez sélectionner les variables à représenter dans le widget, ainsi que les paramètres de graphiques, de couleurs, d’ombres, d’images, etc.

![](/es/_custom/img/monitor_widgets_settings.jpg)

### Variables

Pour introduire des variables dans chacun des widgets, vous pouvez choisir parmi les variables disponibles dans l’arborescence de dispositifs configurée dans l’éditeur.

![](/es/_custom/img/monitor_widgets_varlist.jpg)

Vous pouvez copier le code iframe html public de chaque widget pour l’intégrer dans la page Web souhaitée.

!!! warning "Important"

    Pensez à cliquer sur le bouton "Grabar" (Enregistrer) pour enregistrer vos changements.