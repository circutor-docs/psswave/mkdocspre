---
hide:
  - feedback
---
# Moniteur

Le module Moniteur permet de créer des tableaux de bord avec des widgets pour afficher les informations de votre installation dans une visualisation attirante et intuitive.

Les widgets incorporés dans WAVE permettent de générer facilement des visualisations modernes.