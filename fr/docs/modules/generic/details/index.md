# Informations générales

Dans l’onglet **General (Général)**, vous pouvez modifier les valeurs de base d’un pilote.

![](/es/_custom/img/generic_tab_general_main.jpg)

Dans la section **Opciones (Options)**, les valeurs par défaut sont valides pour la plupart des équipements du marché, à l’exception de ceux dont le manuel stipule le contraire.

Dans la section **Parámetros de pruebas Modbus (Paramètres de tests Modbus)**, vous pouvez configurer les paramètres de la passerelle connectée à l’équipement Modbus, afin de tester les valeurs programmées.