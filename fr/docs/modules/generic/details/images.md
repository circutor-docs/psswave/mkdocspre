# Images

Vous pouvez ajouter ici les images de l’équipement, que vous pourrez ensuite voir dans le client et dans l’éditeur de PowerStudio SCADA. Cette section affecte uniquement l’expérience utilisateur et est donc en cela facultative (si vous n’ajoutez pas d’images, PowerStudio Wave charge l’image de pilote générique par défaut).

![](/es/_custom/img/generic_tab_images_main.jpg)

Les boutons "Importar" (Importer) vous permettent de sélectionner le fichier avec l’image du dispositif.

Pour les utiliser dans l’éditeur et dans le client de PowerStudio, les images doivent être de la taille en pixels indiquée pour chaque option : petite de **16px x 16px**, moyenne de **50px x 50px** et grande de **100px x 100px**.