# Variables numériques

Dans l’onglet **Variables numéricas (Variable numériques)**, vous pouvez saisir toutes les variables numériques de la carte Modbus souhaitées.

![](/es/_custom/img/generic_tab_numvars_main.jpg)

Si vous cliquez sur le bouton ++"Nueva variable" (Nouvelle variable), une boîte de dialogue s’affiche++ pour la configurer.

## General (Général)

![](/es/_custom/img/generic_numvars_general.jpg)

* **Identificador (Identificateur)**: nom avec lequel vous voulez identifier la variable.
* **Nombre (Nom)**: nom de la variable.
* **Descripción (Description)**: informations supplémentaires sur la variable (facultatif).
* **Tipo (Type)**: type de variable. Il peut être de lecture ou d’écriture (information de la carte Modbus du fabricant).
* **Código de función de lectura (Code de fonction de lecture)**: code de fonction pour la lecture de la variable (information de la carte Modbus du fabricant).
* **Código de función d’escritura (Code de fonction d’écriture)**: code de fonction pour l’écriture de la variable (information de la carte Modbus du fabricant).
* **Información de forzado (Informations de forçage)**: informations supplémentaires qui s’affichent quand la variable est forcée (par exemple, des valeurs autorisées pour la variable).
* **Dirección (Adresse)**: adresse de mémoire de la variable (information de la carte Modbus du fabricant).
* **Registros (Registres)**: nombre de registres de la variable  (information de la carte Modbus du fabricant).

## Format et unités

![](/es/_custom/img/generic_numvars_format.jpg)

* **Decimales (Décimales)**: nombre de décimales avec lesquelles est indiquée la variable (information de la carte Modbus du fabricant). Ici il s’agit de 3 car le fabricant indique un facteur de 0,001.
* **IEE754**: sélectionnez cette option quand la variable est de type Float (information de la carte Modbus du fabricant).
* **Mod10**: format de variable Modbus utilisé par certains fabricants. (information de la carte Modbus du fabricant).
* **Con signo (Avec signe)**: indique si la variable a un signe (information de la carte Modbus du fabricant).
* **Unidades (Unités)**: indique les unités de la variable (information de la carte Modbus du fabricant). Lorsque l’unité de figure pas dans la liste déroulante, sélectionnez "Definido por el usuario" (Défini par l'utilisateur) et saisissez-la manuellement.

## Critère de regroupement

Il s’agit du critère selon lequel sont regroupées les données :

* **Valor medio (Valeur moyenne)**: la valeur moyenne des valeurs reçues est conservée.
* **Último valor (Dernière valeur)**: la dernière valeur reçue est conservée.
* **Diferencial (Différentiel)**: la valeur est conservée comme un incrément de la période.

## Métadonnées

![](/es/_custom/img/generic_numvars_metadata.jpg)

Les métadonnées indiquent les informations sur la variable pour que PowerStudio puisse la classer :

* **Familia (Famille)**: indique le type de mesure qui est effectuée (s’il n’est pas disponible dans la liste déroulante, vous pouvant en indiquer un).
* **Orden (Ordre)**: indique l’ordre de la variable (disponible para les variables d’harmoniques).
* **Fase (Phase)**: indique la phase à laquelle appartient la variable.
* **Tipo (Type)**: il s’agit du type de variable, qui peut être : **Instantáneo (Instantané)**: il s’agit d'une variable de type moyenne. **Máximo (Maximum)**: enregistre le maximum de la variable. **Mínimo (Minimum)**: enregistre le minimum de la variable.

## Autres données

![](/es/_custom/img/generic_numvars_others.jpg)

* **Variable analógica (Variable analogique)**: lorsque la variable est un signal analogique, vous pouvez définir le type de signal, la précision, le zéro et la pleine échelle pour mapper le signal et la valeur qu'il représente.
* **Contador (Compteur)**: lorsque la variable est un compteur, vous pouvez indiquer sa valeur maximum pour que PowerStudio la prenne en compte si ce compteur atteint sa limite et se réinitialise.
* **Grupo Modbus (Groupe Modbus)**: indique s'il est nécessaire de lire des variables Modbus comme un groupe.

## Enregistrer et tester

![](/es/_custom/img/generic_numvars_test.jpg)

* **Guardar (Enregistrer)**: indique si les valeurs de cette variable doivent être enregistrées dans la base de données de PowerStudio.
* **Probar (Tester)**: si vous avez configuré correctement la section "Parámetros de pruebas Modbus" (Paramètres de tests Modbus) décrite auparavant en sélectionnant l’icône, vous pourrez voir la valeur de la variable configurée pour vérifier que la programmation est correcte.