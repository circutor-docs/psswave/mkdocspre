# Variables binaires

Dans l’onglet **Variables binárias (Variable binaires)**, vous pouvez saisir toutes les variables binaires du plan Modbus souhaitées.

![](/es/_custom/img/generic_tab_binvars_main.jpg)

Si vous cliquez sur le bouton ++"Nueva variable" (Nouvelle variable), une boîte de dialogue s’affiche++ pour la configurer.

## General (Général)

![](/es/_custom/img/generic_binvars_general.jpg)

* **Identificador (Identificateur)**: nom avec lequel vous voulez identifier la variable.
* **Nombre (Nom)**: nom de la variable.
* **Descripción (Description)**: informations supplémentaires sur la variable (facultatif).
* **Tipo (Type)**: type de variable. Il peut être de lecture ou d’écriture (information de la carte Modbus du fabricant).
* **Código de función de lectura (Code de fonction de lecture)**: code de fonction pour la lecture de la variable (information de la carte Modbus du fabricant).
* **Código de función d’escritura (Code de fonction d’écriture)**: code de fonction pour l’écriture de la variable (information de la carte Modbus du fabricant).

## Métadonnées

![](/es/_custom/img/generic_binvars_metadata.jpg)

Les métadonnées indiquent les informations sur la variable pour que PowerStudio puisse la classer :

* **Familia (Famille)**: indique le type de mesure qui est effectuée (s’il n’est pas disponible dans la liste déroulante, vous pouvant en indiquer un).
* **Orden (Ordre)**: indique l’ordre de la variable (disponible para les variables d’harmoniques).

## Enregistrer et tester

![](/es/_custom/img/generic_binvars_test.jpg)

* **Guardar (Enregistrer)**: indique si les valeurs de cette variable doivent être enregistrées dans la base de données de PowerStudio.
* **Probar (Tester)**: si vous avez configuré correctement la section "Parámetros de pruebas Modbus" (Paramètres de tests Modbus) décrite auparavant en sélectionnant l’icône, vous pourrez voir la valeur de la variable configurée pour vérifier que la programmation est correcte.