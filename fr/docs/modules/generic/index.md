---
hide:
  - feedback
---
# Pilotes génériques

Le module de pilotes génériques permet de créer, d’importer ou de modifier des pilotes pour PowerStudio. Grâce à cet outil, vous pouvez :

- [X] Ajouter de nouveaux pilotes de Circutor sans devoir mettre à jour la version de PowerStudio.
- [X] Modifier les pilotes de Circutor ou non Circutor en n’ajoutant à votre système que les variables dont vous avez besoin.
- [X] Créer vos propres pilotes d’équipements non Circutor, avec une visualisation structurée dans le client de PowerStudio.

## Liste de pilotes

L’écran principal de pilotes génériques inclut la liste des pilotes disponibles, tant ceux officiels de Circutor que ceux d’autres fabricants et ceux personnalisés.

![](/es/_custom/img/generic_list.jpg)

Cet écran présente les données suivantes :

- **Nombre y descripción (Nom et description)**: le nom et la description du pilote. Vous pouvez distinguer un pilote officiel de Circutor d’un pilote externe ou personnalisé. Le nom des pilotes officiels est en gras et assorti d’une icône d’étoile. Ces pilotes ne peuvent être ni modifiés, ni supprimés.

- **No publicado (Non publié)**: si un pilote est indiqué comme "No publicado" (Non publié), il n’est pas disponible dans l’**éditeur** jusqu’à sa publication. Cliquez donc sur ++"Publicar" (Publier)++.

- **Acciones (Actions)**: les actions disponibles pour chaque pilote. Les pilotes officiels de Circutor ne peut être ni modifiés, ni supprimés. Il est en revanche possible de modifier et de supprimer les pilotes externes ou personnalisés.

Par ailleurs, depuis la barre d’outils, vous pouvez créer de nouveaux pilotes, importer des pilotes et en publier ou supprimer par lots.