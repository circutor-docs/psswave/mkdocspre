# Importer un pilote officiel de Circutor

Pour ajouter un nouveau pilote de Circutor, accédez au dispositif depuis la page de produits de Circutor y téléchargez le pilote.

[Pilotes Circutor :octicons-arrow-right-24:](https://circutor.com/){ .md-button .md-button--primary}

Dans PowerStudio WAVE, allez à "Drivers genéricos" (Pilotes génériques) et cliquez sur "Importar" (Importer) dans la barre d’outils.

![](/es/_custom/img/generic_import_button.jpg)

Dans la boîte de dialogue qui s’ouvre, sélectionnez le fichier téléchargé de la page Web de Circutor. Une fois le fichier sélectionné, le pilote est ajouté à la liste (marqué comme pilote officiel) et **vous devez le publier** pour qu’il soit disponible dans l’éditeur en cliquant sur ++"Publicar" (Publier)++.

![](/es/_custom/img/generic_unpublished_detail.jpg)