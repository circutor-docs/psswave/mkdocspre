# Modifier un pilote

Pour conserver le pilote d’origine, vous pouvez faire une copie en cliquant sur "Duplicar" (Dupliquer). Pour modifier un pilote existant, cliquez sur son nom, puis sur ++"Editar" (Éditer) ; l’écran d’édition du pilote s’ouvre alors++.

![](/es/_custom/img/generic_edit_main.jpg)

Dans cet écran, vous pouvez éditer le nom et la description du pilote. Vous disposez aussi de 4 zones pour modifier le pilote :

* **General (Général)**: vous pouvez y modifier les données de base du pilote.
* **Variables númericas (Variables numériques)**: liste des variables numériques. Il s’agit de variables pouvant posséder n'importe quelle valeur numérique.
* **Variables binárias (Variables binaires)**: liste des variables binaires. Il s’agit de variables ne pouvant posséder que deux valeurs : 0 ou 1.
* **Imágenes (Images)**: images qui seront utilisées pour afficher le pilote dans l’éditeur.

## Liste de variables

Les onglets de variables incluent la liste de variables du pilote que vous pouvez modifier de façon individuelle. Vous pouvez également supprimer par lots un groupe de variables sélectionnées en cliquant sur ++"Eliminar n filas" (Supprimer n rangs)++.

![](/es/_custom/img/generic_edit_vars.jpg)

<hr />
Consultez les détails de chaque zone d’édition d'un pilote dans la section [Détails d’un pilote](/es/modules/generic/details).