# Créer un nouveau pilote

Si vous possédez des équipements Circutor, vous pouvez **créer votre propre pilote**. Il vous faut d’abord la **carte mémoire** du fabricant de l’équipement, où est indiquée l’adresse de la variable et ses caractéristiques.

Dans l’exemple suivant, vous pouvez voir la carte Modbus d’un investisseur en photovoltaïque. Dans le manuel de l’investisseur figure l’information suivante :

![](/es/_custom/img/generic_map_demo.jpg)

Après avoir saisi toutes les données de la carte mémoire, il suffit d’**enregistrer** et de **publier** pour que PowerStudio dispose du pilote.

<hr />
Consultez les détails de chaque zone d’édition d'un pilote dans la section [Détails d’un pilote](/es/modules/generic/details/).