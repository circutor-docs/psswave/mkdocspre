# Configuration du moteur

Dans cette section, vous pouvez personnaliser les propriétés du moteur de PowerStudio tout comme il était possible de le faire auparavant avec l’application PSSEngineManager. La section compte trois parties : "Motor" (Moteur), "Autenticación" (Authentification) et "Ruta de los ficheros" (Chemin d’accès aux fichiers).

![](/es/_custom/img/settings_engine_main.jpg)

## Motor (Moteur)

La section "Motor" (Moteur) inclut le nom du moteur et une option pour configurer :

* **Puerto IP (Port IP)**: indique le port que le moteur utilisera pour démarrer le serveur Web. Les demandes de l’éditeur et du client seront traitées sur ce port.
* **Zona horaria por defecto (Fuseau horaire par défaut)**: définit le fuseau horaire dans lequel le moteur travaillera et qui peut être différent de celui du système.

## Autenticación (Authentification)

La section "Autenticación" (Authentification) vous permet de configurer l’utilisateur et le mot de passe si vous souhaitez que l’authentification d’édition soit activée pour le moteur. Si vous n’avez pas besoin de l’authentification d’édition, vous pouvez laisser ces champs vides.

## Ruta de los ficheros (Chemin d’accès aux fichiers)

La section "Ruta de los ficheros" (Chemin d’accès aux fichiers) vous permet de configurer les chemins suivants :

* **Datos (Données)**: répertoire de travail pour stocker les données téléchargées des équipements.
* **Configuración (Configuration)**: répertoire de travail pour stocker la configuration de l’application.
* **Imágenes (Images)**: répertoire de travail pour stocker les images de l’application.

***


Une fois les informations souhaitées modifiées, cliquez sur le bouton "++Guardar++" (Enregistrer) pour appliquer ces changements. Si vous décidez, en cours de modification, de revenir aux valeurs configurées auparavant, cliquez sur le bouton "++Reset++" (Réinitialiser).