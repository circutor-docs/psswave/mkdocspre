# Communications

Dans la section de communications, vous pouvez configurer les propriétés nécessaires pour envoyer des courriers électroniques et des messages à travers de Telegram. Ces services de messagerie sont utilisés dans la section d’événements.

## Serveur de sortie SMTP

![](/es/_custom/img/settings_comms_smtp_main.jpg)

Dans la sous-section "Servidor saliente (SMTP)" (Serveur de sortie (SMTP)), vous pouvez configurer tous les aspects nécessaires pour l’envoi de courriers électroniques :

* **Dirección de correo electrónico (Adresse de courrier électronique)**: adresse du destinataire du courrier.
* **Servidor y puerto (Serveur et port)**: configuration de la communication avec le serveur SMTP d’envoi, comme smtp.outlook365.com et le port 587 pour le serveur SMTP de Outlook.
* **Usuario y contraseña (Utilisateur et mot de passe**: données de connexion pour le serveur SMTP d’envoi de courriers.
* **Utilizar encriptación TLS para la conexión (Utiliser le cryptage TLS pour la connexion)**: sélectionnez cette option si le serveur SMTP permet d’utiliser ce type de cryptage.
* **Habilitado (Habilité)**: permet d’activer ou de désactiver l’envoi de courriers électroniques. Cette option est utile si vous souhaitez désactiver l’envoi de courriers configurés dans les événements à un moment donné.

Pour essayer la configuration d’envoi de courriers avant de l’activer, cliquez sur le bouton "**Probar**" (Tester).

***


Une fois les informations modifiées selon vos préférences, cliquez sur le bouton "**Guardar**" (Enregistrer) pour appliquer ces changements. Si vous décidez, en cours de modification, de revenir aux valeurs configurées auparavant, cliquez sur le bouton "**Reset**" (Réinitialiser).

## Telegram

![](/es/_custom/img/settings_comms_telegram_main.jpg)

Dans la sous-section de configuration de Telegram, vous pouvez faire les réglages suivants :

* **Nombre (Nom)**: nom du bot de Telegram.
* **Identificador de bot de Telegram (Identificateur de bot de Telegram**) : cet identificateur s’utilise en interne avec l’API de Telegram pour envoyer des messages spécifiques.
* **Habilitado (Habilité)**: active ou désactive l’envoi de messages à travers Telegram. Cette option s’avère utile si vous souhaitez par exemple désactiver temporairement l’envoi de messages de Telegram configurés dans les événements.

Pour tester la configuration avant de l’activer, cliquez sur le bouton "**Probar**" (Tester).

***


Après avoir modifié les informations souhaitées, cliquez sur le bouton "**Guardar**" (Enregistrer) pour appliquer ces changements. Si vous souhaitez revenir aux valeurs configurées auparavant lors de la modification, cliquez sur le bouton "**Reset**" (Réinitialiser).

## Configuration pour l’envoi de Telegram

Procédure à suivre pour obtenir les données d’identification de configuration pour l’envoi de Telegram :

### Créer un bot de Telegram et obtenir son botId

* Accédez au navigateur et consultez la page [https://web.telegram.org/a/](https://web.telegram.org/a/ )[ ](https://web.telegram.org/a/ )
* Utilisez l’application Telegram sur votre dispositif mobile. Allez à **Ajustes (Réglages) > Dispositivos (Dispositifs)** et sélectionnez "**Vincular un dispositivo**" (Lier un dispositif).
* Scannez le code QR obtenu dans le navigateur.
* Accédez au navigateur et consultez la page [https://telegram.me/botfather](https://telegram.me/botfather)
* Dans "**BotFather"**, créez un nouveau bot à l’aide de la commande "**/newbot**". Saisissez le nom du bot, par exemple **NOM\_BOT**, et attribuez le nom de l’utilisateur pour le bot, en finissant par "**\_bot**", par exemple "**nom\_bot\_bot**".
* Pour voir tous les bots créés, utilisez dans BotFather la commande "**/mybots**". Pour obtenir le token du bot, vous devez accéder, depuis la liste de bots affichés, à votre bot et cliquer sur l’option "**API Token**" (Token API).
* Pour accéder au bot créé, accédez à l’url **t.me/nomutilisateur\_bot**, où **nomutilisateur\_bot** peut par exemple être **nom\_bot\_bot** (t.me/nom\_bot\_bot).
* Pour démarrer le bot, cliquez sur le bouton "**Start**" (Démarrer) dans son chat.
* Il existe trois destinataires possibles de messages de Telegram : un **utilisateur**, un **groupe** ou un **canal**.
* Pour obtenir l’identificateur d’utilisateur, recherchez **@userinfobot**. Accédez au chat du bot et cliquez sur "**Start**" (Démarrer). L’identificateur de votre utilisateur doit s’afficher et correspond à l’identificateur du destinataire.

### Créer un un canal Telegram pour envoyer des messages avec le bot

* Il est impossible de créer le canal directement depuis Telegram Web.
* Utilisez l’application mobile ou PC pour ajouter le bot au canal en tant qu’administrateur avec des **autorisations d’envoi de messages**.
* Utilisez l’application de bureau pour faciliter la création du canal. Attribuez un nom au canal (par exemple, PSS\_Channel) et configurez-le temporairement comme **público** (public) pour obtenir ensuite le "**channel\_id**».
* Pour ajouter le bot au canal, allez à l’application sur le téléphone et ajoutez-le au canal.
* Une fois le canal créé, envoyez un message au canal via le navigateur avec l’URL ci-après.

```title="URL de ejemplo"
https://api.telegram.org/bot<API_TOKEN>/sendMessage?chat_id=@powerstudiotest&text=TEST 
```

Où **API\_Token** est l’identificateur du bot et "**powerstudiotest**" est le nom du canal

* La réponse doit renvoyer le code **200** et un fichier **JSON**
* Notez la valeur dans le champ "id" dans "chat". Cette valeur correspond au "chat\_id" de la demande.
* Vous pouvez éventuellement repasser le canal à privé.

Vous disposez à présent du **bot\_id** et du **chat\_id** requis pour envoyer des messages à Telegram via l’API.

!!! note "Remarque"

    Si vous voulez créer un groupe depuis Telegram Web, les étapes sont les mêmes que celles de création d’un canal. 