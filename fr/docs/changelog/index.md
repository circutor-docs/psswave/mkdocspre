# Journal des modifications

## 24.1.4 

* Correction d’une erreur dans le service PSS Admin.

## 24.1.3 

* Améliorations de la gestion de la base de données SQL.

## 24.1.2 

* Améliorations de la gestion des communications https.
* Corrections mineures.

## 24.1.1 

* Améliorations de l’outil Moniteur.

## 24.1.0 

* Améliorations de l’optimisation des tables pour la base de données SQL.

## 24.0.1 

* Améliorations de la communications du bus d’événements.

## 24.0.0 

* :party\_popper: :party\_popper: Lancement de PowerStudio Wave !