---


hide:

- navigation
- toc
- feedback
- footer

---


# Documentation de PowerStudio

<div class='card-container'>
<a class='card' href='/fr/home/'>
<img src='/fr/_custom/img/index_powerstudio.jpg' />
<span class='title'>PowerStudio</span>
<span class='content'>Découvrez PowerStudio, le nouvel outil Wave et l’architecture du logiciel.</span>
</a>
<a class='card' href='/fr/quickstart/download/'>
<img src='/fr/_custom/img/index_launch.jpg' />
<span class='title'>Mise en route</span>
<span class='content'>Faites vos premiers pas dans PowerStudio de façon simple et flexible.</span>
</a>
<a class='card' href='/fr/modules/'>
<img src='/fr/_custom/img/index_modules.jpg' />
<span class='title'>Modules</span>
<span class='content'>Découvrez comment configurer et tirer le meilleur profit de chaque fonctionnalité de PowerStudio Wave.</span>
</a>
</div>
